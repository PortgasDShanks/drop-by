<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php'; ?>

<style>
@media screen and (max-width:568px) {
        .ab-content {
            width: 100%;
            float: left;
        }
    }

    @media screen and (max-width:768px) {
        .tab-wrap {
            padding: 2em 0em;
        }

    }

    @media screen and (max-width: 767px) {
        .tab-wrap {
            padding: 2em 0em;
            margin-top: 1em;
        }
    }
    .welcome-msg {
        margin-top: 10%;
    }

    .ab-content {
        width: 50%;
        float: left;
    }
    .ab-content img {
        border-radius: 10px;
        -webkit-border-radius: 10px;
        -o-border-radius: 10px;
        -moz-border-radius: 10px;
        -ms-border-radius: 10px;
        box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.15);
        transition: all 500ms ease;
        width: 100%;
        height: 200px;
        object-fit: cover;
    }

    .ab-content-inner {
        padding: 2em 1em;
    }
    .ab-info {
        text-align: center;
    }

    .ab-info h4 {
        font-size: 1em;
        color: #000000;
        margin: 2em 0 0em 0;
        letter-spacing: 2px;
        text-transform: uppercase;
        font-weight: bolder;
    }
    .ab-info p {
        text-align: center;
        color: #fff;
        font-size: 1em;
        font-weight: 400;
        font-family: 'Oswald', sans-serif;
    }

    .ab-info h5 {
        font-size: 1.1em;
        letter-spacing: 0px;
        font-weight: 700;
        color: #555;
        line-height: 1.7em;
    }

    .ab-info h4 {
        font-size: 0.9em;
        margin: 2em 0 0em 0;
    }

    .tab-wrap {
        padding: 4em 1em;
        border: 1px solid #ddd;
    }

    .tab-wrap {
        padding: 3em 1em;
    }

    p.price {
        /* color: #c20d00; */
        font-weight: 600;
        margin-top: 1em;
    }
.pull-right{
    float: right;
}
.title-name{
    padding: 10px;
    background-color: #3f72afab;
    border-radius: 10px;
    font: message-box;
    text-transform: capitalize;
    font-weight: 700;
    margin-top: 10px;
}
.box1 {
    /* See "NOTE 3" */
    position:relative;
    z-index:1;
    width:100%;
    padding:20px;
    margin:20px auto;
     border: 1px solid rgb(200, 200, 200);
    box-shadow: rgba(0, 0, 0, 0.1) 0px 5px 5px 2px;
	background:  #3f72af;
        border-radius: 4px;
}
.box1 h5 {
    position:relative;
    padding:10px 30px;
    margin:0 -30px 20px;
    font-size:28px;
    line-height:32px;
    font-weight:bold;
    text-align:center;
    color:#fff;
    background:#112d4e;
    /* css3 extras */
    text-shadow:0 1px 1px rgba(0,0,0,0.2);
    -webkit-box-shadow:0 1px 1px rgba(0,0,0,0.2);
       -moz-box-shadow:0 1px 1px rgba(0,0,0,0.2);
            box-shadow:0 1px 1px rgba(0,0,0,0.2);
    /* See "NOTE 1" */
    zoom:1;
}



</style>
<div class="row">
    <div class='col-sm-12'>
        <button onclick="window.location='<?=route('/order')?>'" class='btn btn-sm btn-primary btn-round pull-right'><span class='feather icon-arrow-right'></span> Go to My Orders <span style='font-size: 1.2em;' class="pcoded-badge label label-danger"><?=getPendingOrderCount()?></span></button>
    </div>
    <div class='col-sm-12'>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs md-tabs " role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#home7" role="tab" aria-selected="true">
                <img src="<?= public_url('/storage/images/foodIcon.png') ?>" alt="" style='width: 40px;height: auto;object-fit: cover;'>
               Individual</a>
                <div class="slide"></div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#profile7" role="tab" aria-selected="false">
                <img src="<?= public_url('/storage/images/foodIcon.png') ?>" alt="" style='width: 40px;height: auto;object-fit: cover;'>
                 Promo</a>
                <div class="slide"></div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#messages7" role="tab" aria-selected="false"> <img src="<?= public_url('/storage/images/foodIcon.png') ?>" alt="" style='width: 40px;height: auto;object-fit: cover;'>   Add-ons</a>
                <div class="slide"></div>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content card-block">
            <div class="tab-pane active" id="home7" role="tabpanel">
            <div class='row'>
                <div class="col-sm-12">
                    <div class="grid">
                        <?php  foreach ($individual as $product) { 
                            $status_color = ($product['status'] == 0)?"color: green;":"color: red;";
                            $status_text = ($product['status'] == 0)?"Available":"Not Available";
                        ?>
                            

                        <figure class="effect-winston">
                            <?php if(!empty(getProductImage($product['id']))) { ?>
                                <img src="<?= getImageView(getProdFileType($product['id']), getProductImage($product['id'])) ?>" alt="img21" >
                            <?php } else { ?> 
                                <img alt="NO IMAGE PREVIEW" src="">
                            <?php } ?>
                            <figcaption>
                                <h2><span><?=$product['product_name']?></span></h2>
                                <h2><span>&#8369;  <?=$product['price']?></span></h2>
                                <p>
                                    <a href="#" data-toggle='tooltip' data-placement="top" title="<?=$product['product_desc']?>"><i
                                            class="feather icon-help-circle" style='font-size: 25px;'></i></a>
                                    <a href="#" data-toggle='tooltip' data-placement="top" title="<?=$status_text?>"><i
                                            class="feather icon-info" style='<?=$status_color?>font-size: 25px;'></i></a>
                                    <a href="#" <?=($product['status'] == 0)?"":"style='display: none'"?> onclick="addtoOrder(<?=$product['id']?>, 'I')" data-toggle='tooltip' data-placement="top" title="Click this! if you want to add to your order"><i
                                            class="feather icon-plus-circle" style='font-size: 25px;'></i></a>
                                </p>
                            </figcaption>
                        </figure>
                        <?php } ?>
                    </div>
                </div>
               </div>
            </div>
            <div class="tab-pane" id="profile7" role="tabpanel">
               <div class='row'>
               <div class="col-sm-12">
                    <div class="grid">
                        <?php  foreach ($promo as $products) { 
                            $status_color = ($products['status'] == 0)?"color: green;":"color: red;";
                            $status_text = ($products['status'] == 0)?"Available":"Not Available";
                        ?>
                            

                        <figure class="effect-winston">
                            <?php if(!empty(getProductImagePP($products['id']))) { ?>
                                <img src="<?= getImageView(getProdFileTypePP($products['id']), getProductImagePP($products['id'])) ?>" alt="img21" >
                            <?php } else { ?> 
                                <img alt="NO IMAGE PREVIEW" src="">
                            <?php } ?>
                            <figcaption>
                                <h2><span><?=$products['product_name']?></span></h2>
                                <h2><span>&#8369;  <?=$products['price']?></span></h2>
                                <p>
                                    <a href="#" data-toggle='tooltip' data-placement="top" title="<?=$products['product_desc']?>"><i
                                            class="feather icon-help-circle" style='font-size: 25px;'></i></a>
                                    <a href="#" data-toggle='tooltip' data-placement="top" title="<?=$status_text?>"><i
                                            class="feather icon-info" style='<?=$status_color?>font-size: 25px;'></i></a>
                                    <a href="#" <?=($products['status'] == 0)?"":"style='display: none'"?> onclick="addtoOrder(<?=$products['id']?>, 'PP')" data-toggle='tooltip' data-placement="top" title="Click this! if you want to add to your order"><i
                                            class="feather icon-plus-circle" style='font-size: 25px;'></i></a>
                                </p>
                            </figcaption>
                        </figure>
                        <?php } ?>
                    </div>
                </div>
               </div>
            </div>
            <div class="tab-pane" id="messages7" role="tabpanel">
            <div class='row'>
            <div class="col-sm-12">
                    <div class="grid">
                        <?php  foreach ($addon as $addons) { 
                            $status_color = ($addons['status'] == 0)?"color: green;":"color: red;";
                            $status_text = ($addons['status'] == 0)?"Available":"Not Available";
                        ?>
                            

                        <figure class="effect-winston">
                            <?php if(!empty(getProductImageAO($addons['id']))) { ?>
                                <img src="<?= getImageView(getProdFileTypeAO($addons['id']), getProductImageAO($addons['id'])) ?>" alt="img21" >
                            <?php } else { ?> 
                                <img alt="NO IMAGE PREVIEW" src="">
                            <?php } ?>
                            <figcaption>
                                <h2><span><?=$addons['product_name']?></span></h2>
                                <h2><span>&#8369;  <?=$addons['price']?></span></h2>
                                <p>
                                    <a href="#" data-toggle='tooltip' data-placement="top" title="<?=$addons['product_desc']?>"><i
                                            class="feather icon-help-circle" style='font-size: 25px;'></i></a>
                                    <a href="#" data-toggle='tooltip' data-placement="top" title="<?=$status_text?>"><i
                                            class="feather icon-info" style='<?=$status_color?>font-size: 25px;'></i></a>
                                    <a href="#" <?=($addons['status'] == 0)?"":"style='display: none'"?> onclick="addtoOrder(<?=$addons['id']?>, 'AO')" data-toggle='tooltip' data-placement="top" title="Click this! if you want to add to your order"><i
                                            class="feather icon-plus-circle" style='font-size: 25px;'></i></a>
                                </p>
                            </figcaption>
                        </figure>
                        <?php } ?>
                    </div>
                </div>
               </div>
            </div>
        </div>
    </div>
</div>
<?php include __DIR__ . '/add-to-order-modal.php'; ?>
<script>
    $("#addToOrder").on('submit', function(e){
        e.preventDefault();
        var url = base_url+"/menu/add-to-order";
        var data = $(this).serialize();
        $.post(url, data, function(res){
            $("#orderModal").modal('hide');
            if(res > 0){
                success_query("Successfully Added to Orders");
            }else{
                failed_query();
            }
        });
    });
function getMenus(result){
    $.post(base_url+"/menu/search", {
        result: result
    }, function(res){
        $(".menu-cont").html(res);
    })
}

function operation(op){
    if(op == 'plus'){
        var temp_val = $("#quantity_value").val();

        var final_val = parseInt(temp_val) + 1;

        var puprice = $("#puprice").val();
        var subtotal = puprice * final_val;

        $("#quantity_value").val(final_val);
        $("#totalPrice").val(subtotal);
    }else{
        var temp_val = $("#quantity_value").val();

        var final_val = parseInt(temp_val) - 1;

        if(final_val < 1){
            var final_val2 = 1;
        }else{
            var final_val2 = final_val;
        }

        var puprice = $("#puprice").val();
        var subtotal = puprice * final_val2;

        $("#quantity_value").val(final_val2);
        $("#totalPrice").val(subtotal);
    }
}

function addtoOrder(id, cat){
   $("#orderModal").modal();
   $.post(base_url+"/menu/product-details",{
        id: id,
        cat: cat
   }, function(res){
       console.log(res)
        var data = JSON.parse(res);

        $("#imagePreview").html("<img src='"+data.img+"' alt='No Image Preview' style='width: 100%;height: 200px;border-radius: 10px;object-fit: cover;'>");

        $("#puname").val(data.name);
        $("#pudesc").val(data.desc);
        $("#puprice").val(data.price);
        $("#prodID").val(id);
        $("#category").val(cat);

        $("#totalPrice").val(data.price);
    
   });
}
</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>