<form method="POST" id='addToOrder'>
    <div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="orderModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="orderModalLabel"><span class='fa fa-pencil'></span> Order Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style='max-height: 400px; overflow: auto'>
                    <div class="col-md-12">
                        <div id='imagePreview' style='text-align: center;'></div>
                        <div class="form-group" style='margin-top: 20px'>
                            <label for="username">Menu Name</label>
                            <input readonly type="text" class="form-control" name="puname" id='puname' autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="username">Menu Description</label>
                            <textarea readonly name="pudesc" id='pudesc' style='resize: none' class='form-control' rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="username">Price</label>
                            <input readonly name="puprice" id='puprice' class='form-control' type='number'>
                            <input name="prodID" id='prodID' type='hidden'>
                            <input name="category" id='category' type='hidden'>
                        </div>

                        <div class='col-sm-12'>
                            <div class="input-group input-group-button">
                                <div class="input-group-prepend">
                                    <span onclick='operation("minus")' class="input-group-text btn btn-primary" style='border-radius: 25px 0px 0px 25px' id="basic-addon10">
                                    <span class="feather icon-minus-circle"></span>
                                </span></div>
                                <input type="number" min='1' value='1'  id='quantity_value' name='quantity_value' style='border-radius: 0px 0px 0px 0px' class="form-control" placeholder="Quantity">
                                    <div class="input-group-prepend">
                                    <span onclick='operation("plus")' class="input-group-text btn btn-primary" style='border-radius: 0px 25px 25px 0px' id="basic-addon10">
                                    <span class="feather icon-plus-circle"></span>
                                </span></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="username">Total Price: </label>
                            <input name="totalPrice" id='totalPrice' readonly class='form-control' type='number'>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>