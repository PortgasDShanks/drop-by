<?php

use App\Core\App;
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='icon' href='<?= public_url('/favicon.ico') ?>' type='image/ico' />
    <title>
        <?= ucfirst($pageTitle) . " | " . App::get('config')['app']['name'] ?>
    </title>

    <link rel="stylesheet" href="<?= public_url('/assets/landingPage/css/bootstrap.css') ?>">
    <link rel="stylesheet" href="<?= public_url('/assets/landingPage/css/style.css') ?>">
    <link rel="stylesheet" href="<?= public_url('/assets/landingPage/css/fontawesome-min.css') ?>">
    <style>
        @font-face {
            font-family: Nunito;
            src: url("<?= public_url('/assets/sprnva/fonts/Nunito-Regular.ttf') ?>");
        }

        body {
            font-weight: 300;
            font-family: Nunito;
            color: #26425f;
            background: #eef1f4;
        }

        .bg-light {
            background-color: #ffffff !important;
        }

        .card {
            box-shadow: 0px;
            margin-bottom: 0rem;
            border-radius: 0rem !important;
            border: 1px solid rgba(0, 0, 0, 0.2);
        }

        .wlcm-link {
            text-decoration: underline !important;
            color: inherit;
        }

        .link-green {
            color: #00551f !important;
        }

        .row {
            margin-right: 0px;
            margin-left: 0px;
        }

        .table-bordered {
            border: 1px solid #ccc;
        }

        .table td, .table th {
            padding: 1.50rem;
        }
    </style>

    <script src="<?= public_url('/assets/sprnva/js/jquery-3.6.0.min.js') ?>"></script>
    <script src="<?= public_url('/assets/sprnva/js/popper.min.js') ?>"></script>
    <script src="<?= public_url('/assets/sprnva/js/bootstrap.min.js') ?>"></script>

    <?php
    // this will auto include filepond css/js when adding filepond in public/assets
    if (file_exists('public/assets/filepond')) {
        require_once 'public/assets/filepond/filepond.php';
    }
    ?>

    <script>
        const base_url = "<?= App::get('base_url') ?>";

        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
</head>
</head>

<body>
 <!-- mian-content -->
<div class="main-content" id="home">
    <div class="layer">
        <!-- header -->
        <header>
            <div class="container-fluid px-lg-5">
                <!-- nav -->
                <nav class="py-4 d-lg-flex">
                    <div id="logo">
                        <h1> 
                            <a href="#">
                                <img src="<?=public_url("/storage/images/dropBy_logo.png")?>" alt="LOGO" style="width: 100px;height: auto;object-fit: cover;">
                            Drop By
                            </a>
                        </h1>
                    </div>
                    <label for="drop" class="toggle">Menu</label>
                    <input type="checkbox" id="drop" />
                    <ul class="menu mt-2 ml-auto">
                        <li class="active"><a href="index.html">Home</a></li>
                        <li><a href="#menu" class="scroll">Menu</a></li>
                        <li><a href="#gallery" class="scroll">Gallery</a></li>
                        <!-- <li><a href="#services" class="scroll">Services</a></li> -->
                        <li><a href="#contact" class="scroll">Contact</a></li>
                        <li class="dropdown">
                        <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Portals
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <?php if (fortified()) { ?>
                                <li class="">
                                    <a class="" href="<?= route('/login') ?>"><span class='fas fa-sign-in'></span> Login</a>
                                </li>
                                <li class="">
                                    <a class="" href="<?= route('/register') ?>">Register</a>
                                </li>
                            <?php }else{ ?>
                                <li class="">
                                    <a class="">&nbsp;</a>
                                </li>
                            <?php } ?>
                        </ul>
                        </li>
                    </ul>
                </nav>
                <!-- //nav -->
            </div>
        </header>
        <!-- //header -->
    
        <!-- //banner -->
    </div>
</div>
<!--// mian-content -->
<!--// about -->
<!--/mid-sec-->
<section class="mid-sec py-5" id="menu" style='background-color: #ffffff'>
    <div class="container-fluid py-lg-5">
        <div class="header pb-lg-3 pb-3 text-center">
            <h3 class="tittle mb-lg-3 mb-3">WHAT KIND OF FOOD WE CAN OFFER FOR YOU</h3>
        </div>
        <div class="middile-inner-con">
            <div class="tab-main mx-auto text-center">

                <input id="tab1" type="radio" name="tabs" checked>
                <label for="tab1"><span class="fa fa-align-center" aria-hidden="true"></span> Individual Menu</label>

                <input id="tab2" type="radio" name="tabs">
                <label for="tab2"><span class="fa fa-bolt" aria-hidden="true"></span> Promos</label>

                <input id="tab3" type="radio" name="tabs">
                <label for="tab3"><span class="fa fa-bitbucket" aria-hidden="true"></span> Addons</label>

                <section id="content1">
                    <div class="ab-info row">
                    <?php 
                        $IP = DB()->selectLoop("*", "individual_products")->get();
                        foreach ($IP as $i_prod) {

                    
                    ?>
                        <div class="col-md-3 ab-content">
                            <div class="tab-wrap">
                                <?php if(!empty(getProductImage($i_prod['id']))) { ?>
                                    <img alt="Image placeholder" src="<?= getImageView(getProdFileType($i_prod['id']), getProductImage($i_prod['id'])) ?>" class="img-fluid" style=''>
                                <?php } else { ?> 
                                    <img alt="NO IMAGE PREVIEW" src="" class="img-fluid" style=''>
                                <?php } ?>
                                <div class="ab-info-con">
                                    <h4><?=$i_prod['product_name']?></h4>
                                    <p class="price"> &#8369; <?=$i_prod['price']?></p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </div>

                </section>

                <section id="content2">

                    <div class="ab-info row">
                    <?php 
                        $PP = DB()->selectLoop("*", "promo_products")->get();
                        foreach ($PP as $p_prod) {

                    
                    ?>
                        <div class="col-md-3 ab-content">
                            <div class="tab-wrap">
                                <?php if(!empty(getProductImage($i_prod['id']))) { ?>
                                    <img alt="Image placeholder" src="<?= getImageView(getProdFileTypePP($i_prod['id']), getProductImagePP($i_prod['id'])) ?>" class="img-fluid" style=''>
                                <?php } else { ?> 
                                    <img alt="NO IMAGE PREVIEW" src="" class="img-fluid" style=''>
                                <?php } ?>
                                <div class="ab-info-con">
                                    <h4><?=$p_prod['product_name']?></h4>
                                    <p class="price">  &#8369; <?=$p_prod['price']?></p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                </section>

                <section id="content3">
                    <div class="ab-info row">
                    <?php 
                        $AO = DB()->selectLoop("*", "addons")->get();
                        foreach ($AO as $a_prod) {

                    
                    ?>
                        <div class="col-md-3 ab-content">
                            <div class="tab-wrap">
                                <?php if(!empty(getProductImageAO($a_prod['id']))) { ?>
                                    <img alt="Image placeholder" src="<?= getImageView(getProdFileTypeAO($a_prod['id']), getProductImageAO($a_prod['id'])) ?>" class="img-fluid" style=''>
                                <?php } else { ?> 
                                    <img alt="NO IMAGE PREVIEW" src="" class="img-fluid" style=''>
                                <?php } ?>
                                <div class="ab-info-con">
                                    <h4><?=$a_prod['product_name']?></h4>
                                    <p class="price">&#8369; <?=$a_prod['price']?></p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
<!--//mid-sec-->

<!-- Gallery -->
<section class="gallery py-5" id="gallery">
    <div class="container py-md-5">
        <div class="header text-center">
            <h3 class="tittle mb-lg-5 mb-3">Our Gallery</h3>
        </div>
        <div class="row news-grids text-center gallery-wrap">
            <div class="col-md-3 gal-img">
                <a href="#gal1"><img src="images/g1.jpg" alt="news image" class="img-fluid"></a>
            </div>
            <div class="col-md-3 gal-img">
                <a href="#gal2"><img src="images/g2.jpg" alt="news image" class="img-fluid"></a>
            </div>
            <div class="col-md-3 gal-img">
                <a href="#gal3"><img src="images/g3.jpg" alt="news image" class="img-fluid"></a>
            </div>
            <div class="col-md-3 gal-img">
                <a href="#gal4"><img src="images/g4.jpg" alt="news image" class="img-fluid"></a>
            </div>

        </div>

        <!-- popup-->
        <div id="gal1" class="pop-overlay animate">
            <div class="popup">
                <img src="images/g1.jpg" alt="Popup Image" class="img-fluid" />

                <a class="close" href="#gallery">&times;</a>
            </div>
        </div>
        <!-- //popup -->

        <!-- popup-->
        <div id="gal2" class="pop-overlay animate">
            <div class="popup">
                <img src="images/g2.jpg" alt="Popup Image" class="img-fluid" />

                <a class="close" href="#gallery">&times;</a>
            </div>
        </div>
        <!-- //popup -->
        <!-- popup-->
        <div id="gal3" class="pop-overlay animate">
            <div class="popup">
                <img src="images/g3.jpg" alt="Popup Image" class="img-fluid" />

                <a class="close" href="#gallery">&times;</a>
            </div>
        </div>
        <!-- //popup3 -->
        <!-- popup-->
        <div id="gal4" class="pop-overlay animate">
            <div class="popup">
                <img src="images/g4.jpg" alt="Popup Image" class="img-fluid" />

                <a class="close" href="#gallery">&times;</a>
            </div>
        </div>
        <!-- //popup -->


    </div>
</section>
<!--// gallery -->

<!-- contact -->
<section class="contact py-5" id="contact">
    <div class="container pb-md-5">
        <div class="header py-lg-5 pb-3 text-center">
            <h3 class="tittle mb-lg-5 mb-3"> Contact Us</h3>
        </div>
        <ul class="list-unstyled row text-left mb-lg-5 mb-3">
            <li class="col-lg-4 adress-info">
                <div class="row">
                    <div class="col-md-3 text-lg-center adress-icon">
                        <span class="fa fa-map-marker"></span>
                    </div>
                    <div class="col-md-9 text-left">
                        <h6>Location</h6>
                        <p>Drop By
                            <br>Bacolod City. </p>
                    </div>
                </div>
            </li>

            <li class="col-lg-4 adress-info">
                <div class="row">
                    <div class="col-md-3 text-lg-center adress-icon">
                        <span class="fa fa-envelope-open-o"></span>
                    </div>
                    <div class="col-md-9 text-left">
                        <h6>Email</h6>
                        <a href="mailto:info@example.com">mail@example.com</a>
                        <br>
                        <a href="mailto:info@example.com">mail2@example.com</a>
                    </div>
                </div>
            </li>
            <li class="col-lg-4 adress-info">
                <div class="row">
                    <div class="col-md-3 text-lg-center adress-icon">
                        <span class="fa fa-mobile"></span>
                    </div>
                    <div class="col-md-9 text-left">
                        <h6>Phone Number</h6>
                        <p>+ 1234567890</p>
                        <p>+ 0987654321</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>

</section>
<!-- //contact -->
<!-- footer -->
<footer class="footer-content">
    <div class="layer footer">
        <div class="container-fluid">
            <div class="row footer-top-inner-w3ls">
                <div class="col-lg-4 col-md-6 footer-top mt-md-0 mt-sm-5">
                    <h2>
                        <a href="index.html"><span class="fa fa-align-center" aria-hidden="true"></span>Drop By</a>
                    </h2>
                    <p class="my-3">Donec consequat sam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus</p>
                    <p>
                        Id quod possimusapien ut leo cursus rhoncus. Nullam dui mi, vulputate ac metus at, semper varius orci.
                    </p>
                </div>
                <div class="col-lg-4 col-md-6 mt-md-0 mt-5">
                    <div class="footer-w3pvt">
                        <h3 class="mb-3 w3pvt_title">Opening Hours</h3>
                        <hr>
                        <ul class="list-info-w3pvt last-w3ls-contact mt-lg-4">
                            <li>
                                <p> Monday - Friday 08.00 am - 10.00 pm</p>

                            </li>
                            <li class="my-2">
                                <p>Saturday 08.00 am - 10.00 pm</p>

                            </li>
                            <li class="my-2">
                                <p>Sunday 08.00 am - 10.00 pm</p>

                            </li>


                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mt-lg-0 mt-5">
                    <div class="footer-w3pvt">
                        <h3 class="mb-3 w3pvt_title">Contact Us</h3>
                        <hr>
                        <div class="last-w3ls-contact">
                            <p>
                                <a href="mailto:example@email.com">info@example.com</a>
                            </p>
                        </div>
                        <div class="last-w3ls-contact my-2">
                            <p>+ 456 123 7890</p>
                        </div>
                        <div class="last-w3ls-contact">
                            <p>+ 90 nsequursu dsdesdc,
                                <br>xxx Honey State 049436.</p>
                        </div>
                    </div>
                </div>

            </div>

            <p class="copy-right-grids text-li text-center my-sm-4 my-4">© 2021 Drop By. All Rights Reserved | Design by
                <a href=""> US </a>
            </p>
            <div class="w3ls-footer text-center mt-4">
                <ul class="list-unstyled w3ls-icons">
                    <li>
                        <a href="#">
                            <span class="fa fa-facebook-f"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="fa fa-twitter"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="fa fa-dribbble"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="fa fa-vk"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="move-top text-right"><a href="#home" class="move-top"> <span class="fa fa-angle-up  mb-3" aria-hidden="true"></span></a></div>
        </div>
        <!-- //footer bottom -->
    </div>
</footer>
<!-- //footer -->
</body>

</html>