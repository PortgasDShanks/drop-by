<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php'; ?>

<style>

</style>
<div class="row">
<div class="col-lg-12 col-xl-8">
    <!-- Open position card start -->
    <div class="card">
        <div class="card-header">
            <h5>Add Information to your Order</h5>
        </div>
        <div class="card-block">
        <form method="POST" id='finishOrder'>
           <div class='row'>
                <div class='col-sm-6'>
                    <div class="form-group" style='margin-top: 20px'>
                        <label for="username">Book Type</label>
                        <select name="bookType" id="bookType" class='form-control'>
                            <option value="">&mdash; Please Choose &mdash; </option>
                            <option value="0">Dine-In</option>
                            <option value="1">Takeout</option>
                        </select>
                    </div>
                </div>

                <div class='col-sm-6'>
                    <div class="form-group" style='margin-top: 20px'>
                        <label for="username">Book/Delivery Date</label>
                        <input type="date" class='form-control' id='delDate' name='delDate'>
                    </div>
                </div>

                <div class='col-sm-6'>
                    <div class="form-group" style='margin-top: 20px'>
                        <label for="username">Contact Person</label>
                        <input type="text" class='form-control' name='contactPerson' id='contactPerson'>
                    </div>
                </div>

                <div class='col-sm-6'>
                    <div class="form-group" style='margin-top: 20px'>
                        <label for="username">Contact Number</label>
                        <input type="number" class='form-control' name='contactNumber' id='contactNumber'>
                    </div>
                </div>

                <div class='col-sm-12'>
                    <div class="form-group" style='margin-top: 20px'>
                        <label for="username">Add Note to your order</label>
                        <textarea name="note" id='note' style='resize: none' class='form-control' rows="3"></textarea>
                    </div>
                </div>

                <div class='col-sm-12' >
                   <button class='btn btn-primary btn-round' style='float: right'><span class='feather icon-check-circle'></span> Finish Order</button>
                </div>
           </div>
        </form>
        </div>
    </div>
    <!-- Open position card end -->
</div>    
<div class="col-xs-12 col-sm-4">
    <!-- Filter card start -->
    <div class="card">
        <div class="card-header">
            <h5><i class="icofont icofont-info-circle m-r-5"></i>ORDER AMOUNT SUMMARY</h5>
        </div>
        <div class="card-block">
            <form action="#">
                <div class="form-group row">
                    <div class="col-sm-12" style='text-align: center'>
                        <span style='font-size: 20px;font-weight: bolder;'>&#8369; <?=getCurrentTotalAmount()?></span>
                    </div>
                </div>
                <div class="text-right">
                    <button type="button" onclick='window.location="<?=route("/order")?>"' class="btn btn-primary btn-block btn-round">
                        <i class="icofont icofont-arrow-left m-r-5"></i> Back
                    </button>
                    <button type="button" onclick='window.location="<?=route("/menu")?>"' class="btn btn-primary btn-block btn-round">
                        <i class="icofont icofont-search m-r-5"></i> Continue adding to my order
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>     
   
</div>
<script>
$("#finishOrder").on('submit', function(e){
    e.preventDefault();
    var url = base_url+"/order/finish-order";
    var data = $(this).serialize();
    $.post(url, data, function(res){
        if(res > 0){
            $.confirm({
                icon: 'feather icon-check-circle text-green',
                title: 'Success!',
                type: 'green',
                content: "Order has been successfully set! Status of this transaction will be posted through your email.",
                buttons:{
                Okay: function(){
                    window.location = '<?=route("/transaction")?>';
                }
                }
            });
        }else{
            failed_query();
        }
    });
})
</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>