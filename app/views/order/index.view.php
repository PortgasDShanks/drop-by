<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php'; ?>

<style>

</style>
<div class="row">
<?php if(count($orders) > 0){ ?>
<div class="col-lg-12 col-xl-8">
    <!-- Open position card start -->
    <div class="card">
        <div class="card-header">
            <h5>Your Order</h5>

        </div>
        <div class="card-block">
            <?php 
            

           
            foreach ($orders as $order) { 
                if($order['order_category'] == 'I'){
                    $url = (!empty(getProductImage($order['order_item'])))?getImageView(getProdFileType($order['order_item']), getProductImage($order['order_item'])):"";
                }else if($order['order_category'] == 'PP'){
                    $url = (!empty(getProductImagePP($order['order_item'])))?getImageView(getProdFileTypePP($order['order_item']), getProductImagePP($order['order_item'])):"";
                }else{
                    $url = (!empty(getProductImageAO($order['order_item'])))?getImageView(getProdFileTypeAO($order['order_item']), getProductImageAO($order['order_item'])):"";
                }

                $amount = $order['quantity'] * $order['price'];
            ?>
            <div class="job-cards">
                <div class="media">
                    <a class="media-left media-middle" href="#">
                        <img src="<?= $url ?>" alt="NO IMAGE PREVIEW" >
                        
                    </a>
                    <div class="media-body">
                        <div class="company-name m-b-10">
                            <p><?=getMenuName($order['order_category'], $order['order_item'])?></p>
                        </div>
                        <p class="text-muted">&#8369; <?=number_format($amount, 2)?></p>
                        <div class="col-sm-6">
                            <div class="input-group input-group-button">
                                <div class="input-group-prepend">
                                    <span onclick='operation(<?=$order["id"]?>, "minus")' class="input-group-text btn btn-primary" style='border-radius: 25px 0px 0px 25px' id="basic-addon10">
                                    <span class="feather icon-minus-circle"></span>
                                </span></div>
                                <input type="number" min='1' readonly value='<?=$order['quantity']?>'  id='quantity_value' name='quantity_value' style='border-radius: 0px 0px 0px 0px' class="form-control" placeholder="Quantity">
                                    <div class="input-group-prepend">
                                    <span onclick='operation(<?=$order["id"]?>, "plus")' class="input-group-text btn btn-primary" style='border-radius: 0px 25px 25px 0px' id="basic-addon10">
                                    <span class="feather icon-plus-circle"></span>
                                </span></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php } ?>

        </div>
    </div>
    <!-- Open position card end -->
</div>    
<div class="col-xs-12 col-sm-4">
    <!-- Filter card start -->
    <div class="card">
        <div class="card-header">
            <h5><i class="icofont icofont-info-circle m-r-5"></i>ORDER AMOUNT SUMMARY</h5>
        </div>
        <div class="card-block">
            <form action="#">
                <div class="form-group row">
                    <div class="col-sm-12" style='text-align: center'>
                        <span style='font-size: 20px;font-weight: bolder;'>&#8369; <?=getCurrentTotalAmount()?></span>
                    </div>
                </div>
                
                <div class="text-right">
                    <button type="button" onclick="window.location='<?=route('/order/finalize')?>'" class="btn btn-primary btn-block btn-round">
                        <i class="icofont icofont-check-circled m-r-5"></i> Finalize your order
                    </button>
                    <button type="button" onclick='window.location="<?=route("/menu")?>"' class="btn btn-primary btn-block btn-round">
                        <i class="icofont icofont-search m-r-5"></i> Continue adding to my order
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>  
<?php } else { ?>
    <div class="col-12">
        <div class="d-flex flex-column align-items-center justify-content-center">
            <h2 class="mb-0 text-muted welcome-msg">NO OUTSTANDING ORDER YET...</h2>
            <p class="text-muted">You can either set a <a href="#" data-toggle='modal' data-target='#dineinModal'>Dine-in</a> or go to <a href="#" onclick="window.location='<?=route('/menu')?>'">Menus</a> to choose your order</p>
        </div>
    </div>
<?php } ?>
</div>
<?php include __DIR__ . '/dine-in-modal.php'; ?>
<script>
$("#setDineIn").on('submit', function(e){
    e.preventDefault();
    var url = base_url+"/order/set-dine-in";
    var data = $(this).serialize();
    $.post(url, data, function(res){
        if(res > 0){
            $.confirm({
                icon: 'feather icon-check-circle text-green',
                title: 'Success!',
                type: 'green',
                content: "Dine-in Successfully set, An E-mail will sent to you for the status of your transaction",
                buttons:{
                    Okay: function(){
                        window.location = "<?=route("/transaction")?>";
                    }
                }
            });
        }else{
            failed_query();
        }
    });
});
function operation(id, action){
    $.post(base_url+"/order/change-quantity", {
        id: id,
        action: action
    }, function(res){
        if(res == 1){
            success_query("Quantity Successfully Added");
        }else if(res == 2){
            warning_query("Quantity will be less than 1, Unable to update!");
        }else{
            failed_query();
        }
    });
}
</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>