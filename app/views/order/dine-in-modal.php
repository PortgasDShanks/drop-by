<form method="POST" id='setDineIn'>
    <div class="modal fade" id="dineinModal" tabindex="-1" role="dialog" aria-labelledby="dineinModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="dineinModalLabel"><span class='fa fa-pencil'></span> Set Dine-in</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style='max-height: 400px; overflow: auto'>
                    <div class="col-md-12">
                        <div class="form-group" style='margin-top: 20px'>
                            <label for="username">Contact Person</label>
                            <input type="text" class="form-control" name="contactPerson" id='contactPerson' autocomplete="off">
                        </div>

                        <div class="form-group" style='margin-top: 20px'>
                            <label for="username">Contact Number</label>
                            <input type="number" class="form-control" name="contactNo" id='contactNo' autocomplete="off">
                        </div>

                        <div class="form-group" style='margin-top: 20px'>
                            <label for="username">Date</label>
                            <input type="date" class="form-control" name="bookDate" id='bookDate'>
                        </div>

                        <div class="form-group">
                            <label for="username">Note</label>
                            <textarea name="bookNote" id='bookNote' style='resize: none' class='form-control' rows="3"></textarea>
                        </div>

                       
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>