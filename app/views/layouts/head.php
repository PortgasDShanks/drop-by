<?php

use App\Core\App;
use App\Core\Auth;
?>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel='icon' href='<?= public_url('/favicon.ico') ?>' type='image/ico' />
	<title>
		<?= ucfirst($pageTitle) . " | " . App::get('config')['app']['name'] ?>
	</title>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/bower_components/bootstrap/css/bootstrap.min.css') ?>">

	<link rel="stylesheet" href="<?= public_url('/assets/adminty/assets/icon/feather/css/feather.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/assets/icon/icofont/css/icofont.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/assets/pages/data-table/css/buttons.dataTables.min.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') ?>" type="text/css">

	<link rel="stylesheet" href="<?= public_url('/assets/adminty/bower_components/bootstrap-datepicker/css/bootstrap-datepicker3.standalone.min.css') ?>" type="text/css">

	<link rel="stylesheet" href="<?= public_url('/assets/adminty/assets/css/style.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/assets/css/jquery.mCustomScrollbar.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/jquery-confirm/jquery-confirm.min.css') ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
	<link href='<?= public_url('/assets/adminty/fullcalendar/fullcalendar.min.css') ?>' rel='stylesheet' />
	<link href='<?= public_url('/assets/adminty/fullcalendar/fullcalendar.print.min.css') ?>' media="print" rel='stylesheet' />
	<link href='<?= public_url('/assets/adminty/daterangepicker/daterangepicker-bs3.css') ?>' rel='stylesheet' />

	<link href='<?= public_url('/assets/adminty/menuCss/normalize.css') ?>' rel='stylesheet' />
	<link href='<?= public_url('/assets/adminty/menuCss/hoverSet2.css') ?>' rel='stylesheet' />
	<style>
		.pcoded .pcoded-header[header-theme="theme1"] {
			background: #3F72AF;
			color: #ffffff;
		}

		.pcoded .pcoded-navbar[navbar-theme="theme1"] .main-menu {
			background-color: #112D4E;
		}

		.pcoded .pcoded-header .navbar-logo[logo-theme="theme1"] {
			background-color: #3F72AF;
		}

		.pcoded .pcoded-navbar[navbar-theme="theme1"] {
			background: #112D4E;
		}
	</style>

	<script src="<?= public_url('/assets/adminty/bower_components/jquery/js/jquery.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/jquery-ui/js/jquery-ui.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/popper.js/js/popper.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/bootstrap/js/bootstrap.min.js') ?>"></script>
	
	<script src="<?= public_url('/assets/adminty/bower_components/jquery-slimscroll/js/jquery.slimscroll.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/modernizr/js/modernizr.js') ?>"></script>

	<!-- datatables -->
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/datedropper/js/datedropper.min.js') ?>"></script>

	<script src="<?= public_url('/assets/adminty/assets/js/jquery.mCustomScrollbar.concat.min.js') ?>"></script>
	<!-- <script src="<?= public_url('/assets/adminty/assets/js/SmoothScroll.js') ?>"></script> -->
	<script src="<?= public_url('/assets/adminty/assets/js/pcoded.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/js/vartical-layout.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/js/script.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/js/pcoded.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/jquery-confirm/jquery-confirm.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/fullcalendar/moment.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/fullcalendar/fullcalendar.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/highchart/highcharts1.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/highchart/highcharts-more.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/highchart/exporting.js') ?>"></script>
	<?php require_once __DIR__ . '/filepond.php'; ?>
	<script>
		const base_url = "<?= App::get('base_url') ?>";
	</script>
	<?php
		// this will auto include filepond css/js when adding filepond in public/assets
		if (file_exists('public/assets/filepond')) {
			require_once 'public/assets/filepond/filepond.php';
		}
	?>
</head>

<body>
	<!-- Pre-loader start -->
	<div class="theme-loader">
		<div class="ball-scale">
			<div class='contain'>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Pre-loader end -->

	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">

			<nav class="navbar header-navbar pcoded-header">
				<div class="navbar-wrapper">

					<div class="navbar-logo">
						<a class="mobile-menu" id="mobile-collapse" href="#!">
							<i class="feather icon-menu"></i>
						</a>
						<a href="<?= route('/') ?>">
							<h5></h5>
						</a>
						<a class="mobile-options">
							<i class="feather icon-more-horizontal"></i>
						</a>
					</div>

					<div class="navbar-container container-fluid">
						<ul class="nav-left">
							<li>
								<a href="#!" onclick="javascript:toggleFullScreen()">
									<i class="feather icon-maximize full-screen"></i>
								</a>
							</li>
						</ul>
						<ul class="nav-right">
							<li class="header-notification">
								<div class="dropdown-primary dropdown">
									<div class="dropdown-toggle" data-toggle="dropdown">
										<i class="feather icon-bell"></i>
										<span class="badge bg-c-pink"></span>
									</div>
									<ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
										<li>
											<h6>Notifications</h6>
										</li>
										
											<li>
												<div class="media" onclick="">
													<div class="media-body">
														<h5 class="notification-user"></h5>
														<p class="notification-msg"></p>
														<span class="notification-time"></span>
													</div>
												</div>
											</li>
									
									</ul>
								</div>
							</li>
							<!-- <li class="header-notification" style=''>
								<div class="dropdown-primary dropdown">
									<div class="displayChatbox dropdown-toggle" data-toggle="dropdown">
										<i class="feather icon-message-square"></i>
										<span class="badge bg-c-pink"></span>
									</div>
								</div>
							</li> -->
							<li class="user-profile header-notification">
								<div class="dropdown-primary dropdown">
									<div class="dropdown-toggle" data-toggle="dropdown">
								
										<?php if(empty(getAvatar())){ ?>
											
											<img src="<?= public_url('/storage/images/avatar.png') ?>" class="img-radius" alt="User-Profile-Image">
										<?php } else { ?>
											
											<img src="<?= getImageView(getFileType(), getAvatar()) ?>" class="img-radius" alt="User-Profile-Image" style='object-fit: cover'>
										<?php } ?>
										<span><?= Auth::user('fullname') ?></span>
										
										<i class="feather icon-chevron-down"></i>
									</div>
									<ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
										<!-- <li>
											<a href="#!">
												<i class="feather icon-settings"></i> Settings
											</a>
										</li> -->
										<li>
											<a href="<?= route('/profile') ?>">
												<i class="feather icon-user"></i> Profile
											</a>
										</li>
										<li>
											<a href="<?= route('/logout') ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
												<i class="feather icon-log-out"></i> Logout
											</a>

											<form id="logout-form" action="<?= route('/logout') ?>" method="POST" style="display:none;">
												<?= csrf() ?>
											</form>
										</li>
									</ul>

								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>

			<!-- Sidebar chat start -->
		

			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<nav class="pcoded-navbar">
						<div class="pcoded-inner-navbar main-menu">
							<div class="pcoded-navigatio-lavel">Navigation</div>
							<ul class="pcoded-item pcoded-left-item">
								<li class="">
									<a href="<?= route('/home') ?>">
										<span class="pcoded-micon"><i class="feather icon-grid"></i></span>
										<span class="pcoded-mtext">Dashboard</span>
									</a>
								</li>
								<!-- <li class="">
									<a href="<?= route('/hospitals') ?>">
										<span class="pcoded-micon"><i class="feather icon-home"></i></span>
										<span class="pcoded-mtext">Hospitals</span>
									</a>
								</li> -->
								<?php if(Auth::user('role_id') == 'A') { ?>
							
								<li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style1">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-list"></i></span>
                                        <span class="pcoded-mtext">Products</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="">
                                            <a href="<?= route('/products/individual') ?>">
                                                <span class="pcoded-mtext">Individual Products</span>&nbsp;
												<span class="pcoded-badge"> </span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="<?= route('/products/promo') ?>">
                                                <span class="pcoded-mtext">Promo Products</span>
                                            </a>
                                        </li>
										<li class="">
                                            <a href="<?= route('/products/add-ons') ?>">
                                                <span class="pcoded-mtext">Add-ons</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

								<li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style1">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-list"></i></span>
                                        <span class="pcoded-mtext">Transaction</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="">
                                            <a href="<?= route('/transaction/takeout') ?>">
                                                <span class="pcoded-mtext">Take Out</span>&nbsp;
												<span class="pcoded-badge"> </span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="<?= route('/transaction/dinein') ?>">
                                                <span class="pcoded-mtext">Dine In</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

								<li class="">
									<a href="<?= route('/reports') ?>">
										<span class="pcoded-micon"><i class="feather icon-bar-chart"></i></span>
										<span class="pcoded-mtext">Reports</span>
									</a>
								</li>
								<?php } ?>
								<?php if(Auth::user('role_id') == 'U') { ?>
								<li class="">
									<a href="<?= route('/order') ?>">
										<span class="pcoded-micon"><i class="feather icon-list"></i></span>
										<span class="pcoded-mtext">My Orders</span>
									</a>
								</li>

								<li class="">
									<a href="<?= route('/menu') ?>">
										<span class="pcoded-micon"><i class="feather icon-menu"></i></span>
										<span class="pcoded-mtext">Menus</span>
									</a>
								</li>

								<li class="">
									<a href="<?= route('/transaction') ?>">
										<span class="pcoded-micon"><i class="feather icon-menu"></i></span>
										<span class="pcoded-mtext">Transactions</span>
									</a>
								</li>
							
								<?php } ?>
							</ul>
						</div>
					</nav>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<!-- Page-header start -->
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4><?=($pageTitle == "Home")?"":$pageTitle?></h4>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- Page-header end -->
									<div class="page-body">