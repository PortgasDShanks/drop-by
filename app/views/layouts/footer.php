</div>
</body>
<script>
    function success_query(message){
        $.confirm({
            icon: 'feather icon-check-circle text-green',
            title: 'Success!',
            type: 'green',
            content: message,
            buttons:{
              Okay: function(){
                window.location.reload();
              }
            }
        });
    }
    function failed_query(){
        $.confirm({
            icon: 'feather icon-x text-red',
            title: 'Error Encountered!',
            type: 'red',
            content: 'There was an error while saving the data',
            buttons:{
              Okay: function(){
                window.location.reload();
              }
            }
        });
    }
    function warning_query(message){
        $.confirm({
            icon: 'feather icon-alert-circle text-orange',
            title: 'Warning!',
            type: 'orange',
            content: message,
            buttons:{
              Okay: function(){
                window.location.reload();
              }
            }
        });
    }
</script>
</html>