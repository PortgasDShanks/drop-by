<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php'; ?>

<style>

</style>
<div class="row">
<div class='col-sm-12'>
    <div id='chart' style='width:100% !important; margin-top: 20px;'>
                            
    </div>
</div>

</div>
<script>
    $(document).ready( function(){
        generateGraph();
    });
function generateGraph(){
    $("#chart").html("<span class='fa fa-spin fa-spinner'></span>").css("text-align", "center").css("font-size", "50px");

    $.ajax({
        type: "POST",
        url: base_url + "/reports/sales-report",
        data: {},
        dataType: "json",
        success: function (data) {
            console.log(data)
            Highcharts.chart("chart", {
            chart: {
                type: 'column'
            },
            title: {
                useHTML: true,
                text: "Monthly Sales Report"
            },
            xAxis: {
                type: 'category',
                title: {
                text: "Sales Per Month"
                }
            },
            yAxis: {
                title: {
                text: 'Amount (in Peso)'
                }
            },
            plotOptions: {
                column: {
                    dataLabels: {
                    enabled: true
                    },
                    enableMouseTracking: true
                },
                series: {
                    connectNulls: true
                }
            },

            tooltip: {
                headerFormat: '',
                pointFormat: '<span style="color:{point.color}">{point.title} </span> <span style="font-size:11px">{series.title}</span>: <b>{point.y:.0f}</b><br/>'
            },

            series: data['series']
            });
        },
        error: function (data) {
                alert(data);
            }
    })
}
</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>