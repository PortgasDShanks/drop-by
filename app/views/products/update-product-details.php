<form method="POST" id='updateIP'>
    <div class="modal fade" id="updateIPMember" tabindex="-1" role="dialog" aria-labelledby="updateIPMemberLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="updateIPMemberLabel"><span class='fa fa-pencil'></span> Update Individual Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="username">Product Name</label>
                            <input type="text" class="form-control" name="puname" id='puname' autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="username">Product Desc</label>
                            <textarea name="pudesc" id='pudesc' style='resize: none' class='form-control' rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="username">Price</label>
                            <input name="puprice" id='puprice' class='form-control' type='number'>
                            <input name="prodID" id='prodID' type='hidden'>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>