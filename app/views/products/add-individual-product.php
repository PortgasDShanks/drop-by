<form method="POST" id='saveIP'>
    <div class="modal fade" id="addIPMember" tabindex="-1" role="dialog" aria-labelledby="addIPMemberLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addIPMemberLabel"><span class='fa fa-check-circle'></span> Add New Menu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="username">Menu Name</label>
                            <input type="text" class="form-control" name="pname" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="username">Menu Description</label>
                            <textarea name="pdesc" style='resize: none' class='form-control' rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="username">Price</label>
                            <input name="pprice" class='form-control' type='number'>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>