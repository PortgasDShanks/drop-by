<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php'; ?>

<style>
    @media screen and (max-width:568px) {
        .ab-content {
            width: 100%;
            float: left;
        }
    }

    @media screen and (max-width:768px) {
        .tab-wrap {
            padding: 2em 0em;
        }

    }

    @media screen and (max-width: 767px) {
        .tab-wrap {
            padding: 2em 0em;
            margin-top: 1em;
        }
    }
    .welcome-msg {
        margin-top: 10%;
    }

    .ab-content {
        width: 50%;
        float: left;
    }
    .ab-content img {
        border-radius: 10px;
        -webkit-border-radius: 10px;
        -o-border-radius: 10px;
        -moz-border-radius: 10px;
        -ms-border-radius: 10px;
        box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.15);
        transition: all 500ms ease;
        width: 100%;
        height: 200px;
        object-fit: cover;
    }

    .ab-content-inner {
        padding: 2em 1em;
    }
    .ab-info {
        text-align: center;
    }

    .ab-info h4 {
        font-size: 1em;
        color: #000000;
        margin: 2em 0 0em 0;
        letter-spacing: 2px;
        text-transform: uppercase;
        font-weight: bolder;
    }
    .ab-info p {
        text-align: center;
        color: #fff;
        font-size: 1em;
        font-weight: 400;
        font-family: 'Oswald', sans-serif;
    }

    .ab-info h5 {
        font-size: 1.1em;
        letter-spacing: 0px;
        font-weight: 700;
        color: #555;
        line-height: 1.7em;
    }

    .ab-info h4 {
        font-size: 0.9em;
        margin: 2em 0 0em 0;
    }

    .tab-wrap {
        padding: 4em 1em;
        border: 1px solid #ddd;
    }

    .tab-wrap {
        padding: 3em 1em;
    }

    p.price {
        color: #c20d00;
        font-weight: 600;
        margin-top: 1em;
    }
    .pull-right{
        float: right;
    }

    .product-list{
        margin-top: 20px;
    }
</style>
<div class="row">
    <div class='col-sm-12'>
        <button class='btn btn-sm btn-primary btn-round pull-right' data-toggle='modal' data-target='#addIPMember'><span class='feather icon-plus-circle'></span> Add New</button>
    </div>
    <div class='col-sm-12 product-list'>
        <div class='ab-info row'>
        <?php 
        foreach ($products as $product) { ?>
        
            <div class="col-sm-3 ab-content">
                
                <div class="tab-wrap">
                    <span class="show-onhover">
                        <i class="feather icon-settings show-onhover text-muted" style="font-size: 14px;cursor: pointer;float: right" onclick="productOptions(<?=$product['id']?>)"></i>
                    </span> 
                    <?php if(!empty(getProductImageAO($product['id']))) { ?>
                        <img alt="Image placeholder" src="<?= getImageView(getProdFileTypeAO($product['id']), getProductImageAO($product['id'])) ?>" class="" style='margin-top: 10px'>
                    <?php } else { ?> 
                        <img alt="NO IMAGE PREVIEW" src="" class="" style='margin-top: 10px'>
                    <?php } ?>
                    <!-- <img src="images/6.jpg" alt="news image" class="img-fluid"> -->
                    <div class="ab-info-con">
                        <h4><?=$product['product_name']?></h4>
                        <p class="price">&#8369; <?=$product['price']?></p>
                        <p class="availability" style='font-weight: 600;'><?=($product['status'] == 0)?"<span style='color: green; cursor: pointer' data-toggle='tooltip' data-placement='top' title='This Product is Available. Click if you want to set this to NOT AVAILABLE' onclick='setToNA(".$product['id'].")'>Available</span>":"<span style='color: red; cursor: pointer' data-toggle='tooltip' data-placement='top' title='This Product is Not Available. Click if you want to set this to AVAILABLE' onclick='setToA(".$product['id'].")'>Not Available</span>"?></p>
                    </div>
                   
                </div>
            </div>
        
        <?php } ?>
        </div>
    </div>
</div>
<?php include __DIR__ . '/add-individual-product.php'; ?>
<?php include __DIR__ . '/add-images-products.php'; ?>
<?php include __DIR__ . '/update-product-details.php'; ?>
<script>
function donePosting(){
    $("#add_post_modal").modal('hide');
    $.confirm({
        icon: 'fas fa-check-circle text-green',
        title: 'Success!',
        content: "Your Image has been loaded!",
        buttons:{
            Okay: function(){
                window.location.reload();
            }
        }
    });
}
function setToNA(id){
    $.confirm({
        columnClass: 'col-md-6 col-md-offset-3',
        icon: "feather icon-alert-circle text-red",
        title: "CONFIRMATION",
        content: "Are you sure to update the availability of this product?",
        buttons: {
            info: {
                text: "No",
                btnClass: 'btn-red',
                action: function(){
                    $.alert("Action Aborted!");
                }
            },
            danger: {
                text: "Yes",
                btnClass: 'btn-success any-other-class',
                action: function(){
                    var action = "NA";
                    $.post(base_url+"/products/change-status-AO",{
                        id: id,
                        action: action
                    }, function(res){
                        if(res > 0){
                            success_query("Successfully updated status availability");
                        }else{
                            failed_query()
                        }
                    });
                }
            
            }
        }
    }); 
   
}

function setToA(id){
    $.confirm({
        columnClass: 'col-md-6 col-md-offset-3',
        icon: "feather icon-alert-circle text-red",
        title: "CONFIRMATION",
        content: "Are you sure to update the availability of this product?",
        buttons: {
            info: {
                text: "No",
                btnClass: 'btn-red',
                action: function(){
                    $.alert("Action Aborted!");
                }
            },
            danger: {
                text: "Yes",
                btnClass: 'btn-success any-other-class',
                action: function(){
                    var action = "A";
                    $.post(base_url+"/products/change-status-AO",{
                        id: id,
                        action: action
                    }, function(res){
                        if(res > 0){
                            success_query("Successfully updated status availability");
                        }else{
                            failed_query()
                        }
                    });
                }
            
            }
        }
    }); 
}
function updateDetail_showModal(id){

    $.post(base_url+"/products/get-product-details-AO", {
        id: id
    }, function(res){
        $("#updateIPMember").modal();
        var d = JSON.parse(res);
        $("#puname").val(d.name);
        $("#pudesc").val(d.desc);
        $("#puprice").val(d.price);
        $("#prodID").val(id);
    })
}

function updateImage_showModal(id){
    $("#add_post_modal").modal();

    FilePond.setOptions({
        server: {
            url: base_url + "/products/update-image-AO/" + id,
            headers: {
                'X-CSRF-TOKEN': '<?= Request::csrf_token() ?>'
            }
        }
    });

    FilePond.registerPlugin(
        FilePondPluginFileEncode,
        FilePondPluginFileValidateSize,
        FilePondPluginImageExifOrientation,
        FilePondPluginImagePreview
    );

    FilePond.create(document.querySelector('input[type="file"]'));
}

function delete_product(id){
    $.post(base_url+"/products/delete-AO", {
        id: id
    }, function(res){
        if(res > 0){
            success_query("Product Successfully Deleted!");
        }else{
            failed_query();
        }

        $(".ab-info").load(location.href + " .ab-info");
    });
}
function productOptions(id){
    $.confirm({
        icon: 'feather icon-alert-circle',
        title: 'Confirmation',
        content: 'Please choose an option below!',
        type: 'orange',
        columnClass: 'col-md-6 col-md-offset-3',
        closeIcon: true,
        buttons: {
            confirm: {
                text: 'Update Details',
                btnClass: 'btn-green',
                action: function(){
                    updateDetail_showModal(id);
                }
            },
            danger: {
                text: 'Upload Image',
                btnClass: 'btn-blue',
                action: function(){
                    updateImage_showModal(id);
                }
            },

            somethingElse: {
                text: 'Delete Product',
                btnClass: 'btn-red',
                keys: ['enter', 'shift'],
                action: function(){
                    delete_product(id);
                }
            }
        }
    });
}

$("#updateIP").on('submit', function(e){
    e.preventDefault();
    var url = base_url+"/products/update-AO";
    var data = $(this).serialize();
    $.post(url, data, function(res){
        if(res > 0){
            success_query("Product Successfully Updated");
        }else{
            failed_query();
        }
        $("#updateIPMember").modal('hide');
        $(".ab-info").load(location.href + " .ab-info");
    });
});
$("#saveIP").on('submit', function(e){
    e.preventDefault();
    var url = base_url+"/products/add-addons";
    var data = $(this).serialize();
    $.post(url, data, function(res){
        $("#addIPMember").modal('hide');
        if(res > 0){
            $.confirm({
                columnClass: 'col-md-6 col-md-offset-3',
                icon: "feather icon-alert-circle text-red",
                title: "CONFIRMATION",
                content: "Before finishing up, Would you like to add Images on this product?",
                buttons: {
                    info: {
                        text: "No",
                        btnClass: 'btn-red',
                        action: function(){
                            window.location.reload();
                        //$.alert("Individual Product Successfully Added!");
                        }
                    },
                    danger: {
                        text: "Yes",
                        btnClass: 'btn-success any-other-class',
                        action: function(){
                        $("#testid").val(res);
                        $("#add_post_modal").modal();

                    

                            FilePond.setOptions({
                                server: {
                                    url: base_url + "/products/add-image-AO/" + res,
                                    headers: {
                                        'X-CSRF-TOKEN': '<?= Request::csrf_token() ?>'
                                    }
                                }
                            });

                            FilePond.registerPlugin(
                                FilePondPluginFileEncode,
                                FilePondPluginFileValidateSize,
                                FilePondPluginImageExifOrientation,
                                FilePondPluginImagePreview
                            );

                            FilePond.create(document.querySelector('input[type="file"]'));
                        }
                    
                    }
                }
            }); 
        }else{
            failed_query();
        }
    });
});
</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>