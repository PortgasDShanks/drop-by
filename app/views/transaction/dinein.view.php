<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php'; ?>

<style>

</style>
<div class="row">
<div class='col-sm-12'>
<div class="card">
    <div class="card-block">
        <div class="dt-responsive table-responsive">
            <div class="row">
                <div class="col-xs-12 col-sm-12" style='margin-top: 20px;'>
                    <table id="simpletable" class="table table-striped table-bordered nowrap dataTable" role="grid" aria-describedby="simpletable_info">
                        <thead>
                            <tr role="row">
                                <th>#</th>
                                <th>Customer</th>
                                <th>Book Date</th>
                                <th>Note</th>
                                <th>Contact Person</th>
                                <th>Contact Number</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $count = 1; foreach ($transaction as $transactions) { 
                                $color = ($transactions['book_type'] == 0)?"style='background-color: #3f72af'":"style='background-color: #112d4e'";
                            ?>
                            <tr role="row" class="odd" <?=$color?>>
                                <td style='color: #ffffff'><?=$count++;?></td>
                                <td style='color: #ffffff'><?=getCustomerName($transactions['user_id'])?></td> 
                                <td style='color: #ffffff'><?=date("F d, Y", strtotime($transactions['book_date']))?></td>
                                <td style='color: #ffffff'><?=$transactions['note']?></td>
                                <td style='color: #ffffff'><?=$transactions['contact_person']?></td>
                                <td style='color: #ffffff'><?=$transactions['contact_no']?></td>
                                <td>
                                    <?=($transactions['status'] == 0)?"<span style='color: orange'>PENDING</span>":(($transactions['status'] == 1)?"<span style='color: green'>APPROVED</span>":(($transactions['status'] == 3)?"<span style='color: green'>COMPLETED</span>":"<span style='color: red'>CANCELLED</span>"))?>
                                </td>
                                <td>
                                   <button <?=($transactions['status'] == 0)?"":"style='display:none'"?> class='btn btn-sm btn-primary btn-round' onclick="changeStatus(<?=$transactions['id']?>, 'A')"><span class='feather icon-thumbs-up'></span></button>

                                   <button <?=($transactions['status'] == 0)?"":"style='display:none'"?> class='btn btn-sm btn-danger btn-round' onclick="changeStatus(<?=$transactions['id']?>, 'C')"><span class='feather icon-slash'></span></button>

                                   <button <?=($transactions['status'] == 1)?"":"style='display:none'"?> class='btn btn-sm btn-success btn-round' onclick="changeStatus(<?=$transactions['id']?>, 'F')"><span class='feather icon-check-circle'></span></button>
                                </td>
                            </tr>
                            <?php  } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
<script>
$(document).ready( function(){
    $("#simpletable").DataTable();
})

function changeStatus(id, action){
    $.post(base_url+"/transaction/change-status", {
        id: id,
        action: action
    }, function(res){
        success_query("Status Updated! E-mail was sent to customer for details");
    });
}
</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>