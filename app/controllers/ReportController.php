<?php

namespace App\Controllers;
use App\Core\App;
use App\Core\Auth;
use App\Core\Request;
use App\Core\Filesystem;

class ReportController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Monthly Sales Report";


        return view('/reports/index', compact('pageTitle'));
    }

    public function sales()
    {

        $header['series'] = array();
        $data['name'] = "Months";
        $data['colorByPoint'] = true;
        $data['data'] = array();

        $months = array(
            "January" => 1,
            "February" => 2,
            "March" => 3,
            "April" => 4,
            "May" => 5,
            "June" => 6,
            "July" => 7,
            "August" => 8,
            "September" => 9,
            "October" => 10,
            "November" => 11,
            "December" => 12
        );

        foreach($months as $name => $number){
              
            $list = array();
            $list['name'] = $name; 

            $detail = DB()->select("sum(quantity*price) as tamnt","transaction as t, orders as o","t.id = o.header_id AND t.status = 3 AND MONTH(t.book_date) = '$number'")->get();

            if($detail['tamnt'] == 0 || $detail['tamnt'] == null || $detail['tamnt'] == ''){
                $total = 0;
            }else{	
                $total = $detail['tamnt'] * 1;
            }

            $list['y'] = $total;
            array_push($data['data'], $list);
        }
        array_push($header['series'], $data);
		echo json_encode($header);
    }

}
