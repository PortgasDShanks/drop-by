<?php

namespace App\Controllers;
use App\Core\App;
use App\Core\Auth;
use App\Core\Request;
use App\Core\Filesystem;

class MenuController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Menu";

        $individual = DB()->selectLoop("*","individual_products")->get();
        $promo = DB()->selectLoop("*","promo_products")->get();
        $addon = DB()->selectLoop("*","addons")->get();

        return view('/menu/index', compact('pageTitle', 'individual', 'promo', 'addon'));
    }

    public function searchMenu()
    {
        $request = Request::validate();

        $where = ($request['result'] == '')?"":" product_name LIKE '%$request[result]%'";

        $menu = DB()->selectLoop("*","individual_products", $where)->get();
        $content = "";
        if(count($menu) > 0){
        foreach ($menu as $menus) {
            $content .= '<div class="col-sm-4 ab-content row">';
                        
                $content .= '<div class="tab-wrap">';
                   
                   if(!empty(getProductImage($menus['id']))) { 
                        $content .= '<img alt="Image placeholder" src="'. getImageView(getProdFileType($menus['id']), getProductImage($menus['id'])).'" class="" style="margin-top: 10px;object-fit: cover">';
                    } else { 
                        $content .= '<img alt="NO IMAGE PREVIEW" src="" class="" style="margin-top: 10px">';
                    } 
                    $content .= '<div class="ab-info-con">';
                        $content .= '<h4 class="title-name">'.$menus['product_name'].'</h4>';
                        $content .= '<p class="price title-name">&#8369; '.$menus['price'].'</p>';
                        $content .= '<div class="box1"><h5 class="" >'.($menus['status'] == 0)?"<span style='color: green; cursor: pointer'>Available</span>":"<span style='color: red; cursor: pointer'>Not Available</span>".'</h5></div>';
                        $content .= '<button style="'.($menus['status'] == 0)?"":"display:none".'" class="btn btn-sm btn-primary btn-block btn-round pull-right"><span class="feather icon-plus-circle"></span> Add To Cart</button>';
                    $content .= '</div>';
                
                $content .= ' </div>';
            $content .= '</div>';
        }
        }else{ 
            $content .= "<div><h4>Menu Not Found!</h4></div>";
        }
        echo $content;
    }

    public function storeOrder()
    {
        $request = Request::validate();
        $user_id = Auth::user('id');
        $data = [
            "user_id" => $user_id,
            "order_item" => $request['prodID'],
            "order_category" => $request['category'],
            "quantity" => $request['quantity_value'],
            "price" => $request['puprice'],
            "header_id" => 0,
            "status" => 0
        ];

        $response = DB()->insert("orders", $data);

        echo $response;
    }

    public function prodDetails()
    {
        $request = Request::validate();

        $table = ($request['cat'] == 'I')?"individual_products":(($request['cat'] == 'PP')?"promo_products":"addons");

        $product = DB()->select("*", $table, "id = '$request[id]'")->get();

        $response = [];

        if($request['cat'] == 'I'){
            $src = getImageView(getProdFileType($product['id']), getProductImage($product['id']));
        }else if($request['cat'] == 'PP'){
            $src = getImageView(getProdFileTypePP($product['id']), getProductImagePP($product['id']));
        }else{
            $src = getImageView(getProductImageAO($product['id']), getProdFileTypeAO($product['id']));
        }

        $response['name'] = $product['product_name'];
        $response['desc'] = $product['product_desc'];
        $response['price'] = $product['price'];
        $response['img'] = $src;
        $response['category'] = $request['cat'];

        echo json_encode($response);
    }
}
