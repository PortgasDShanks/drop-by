<?php

namespace App\Controllers;
use App\Core\App;
use App\Core\Auth;
use App\Core\Request;
use App\Core\Filesystem;

class OrderController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "My Orders";
        $auth = Auth::user('id');
        $orders = DB()->selectLoop("*", "orders", " user_id = '$auth' AND status = 0")->get();

        return view('/order/index', compact('pageTitle', 'orders'));
    }

    public function finalizeOrder()
    {
        $pageTitle = "Finalize Order";

        return view('/order/finalize', compact('pageTitle'));
    }

    public function changeQntty()
    {
        $request = Request::validate();

        $item = DB()->select("*", "orders", "id = '$request[id]'")->get();

        $quantity = $item['quantity'];

        if($quantity == 1 && $request['action'] == 'minus'){
            $response = 2;
        }else{
            $new_qntty = ($request['action'] == 'plus')?$quantity + 1:$quantity - 1;

            $data = [
                "quantity" => $new_qntty
            ];

            $response = DB()->update("orders", $data, "id = '$request[id]'");
        }

        echo $response;
    }

    public function finishOrder()
    {
        $user = Auth::user('id');
        $request = Request::validate();

        $data = [
            "user_id" => $user,
            "book_type" => $request['bookType'],
            "contact_person" => $request['contactPerson'],
            "contact_no" => $request['contactNumber'],
            "note" => $request['note'],
            "status" => 0,
            "book_date" => $request['delDate']
        ];

        $response = DB()->insert('transaction', $data, "Y");

        if($response > 0){
            $data = [
                "status" => 1,
                "header_id" => $response
            ];

            $orders = DB()->update("orders", $data, " user_id = '$user' AND status = 0 AND header_id = 0");
        }

        echo $orders;
    }

    public function setDineIn()
    {
        $user = Auth::user('id');
        $request = Request::validate();

        $data = [
            "user_id" => $user,
            "book_type" => 0,
            "contact_person" => $request['contactPerson'],
            "contact_no" => $request['contactNo'],
            "note" => $request['bookNote'],
            "status" => 0,
            "book_date" => $request['bookDate']
        ];

        $response = DB()->insert("transaction", $data);

        echo $response;
    }
}
