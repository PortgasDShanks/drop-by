<?php

namespace App\Controllers;
use App\Core\App;
use App\Core\Auth;
use App\Core\Request;
use App\Core\Filesystem;

class TransactionController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "All Transactions";
        $auth = Auth::user('id');
        $transaction = DB()->selectLoop("*", "transaction", " user_id = '$auth' ORDER BY date_added DESC")->get();

        return view('/transaction/index', compact('pageTitle', 'transaction'));
    }

   public function takeout()
   {
    $pageTitle = "Takeout Transactions";
    $transaction = DB()->selectLoop("*", "transaction", "book_type = 1 ORDER BY date_added DESC")->get();

    return view('/transaction/takeout', compact('pageTitle', 'transaction'));
   }

   public function dinein()
   {
    $pageTitle = "Dine-In Transactions";
    $transaction = DB()->selectLoop("*", "transaction", "book_type = 0 ORDER BY date_added DESC")->get();

    return view('/transaction/dinein', compact('pageTitle', 'transaction'));
   }

   public function changeStatus()
   {
       $request = Request::validate();

       $status = ($request['action'] == 'A')?"1":(($request['action'] == 'C')?"4":(($request['action'] == 'F')?"3":"2"));

       $data = [
           "status" => $status
       ];

       $response = DB()->update("transaction", $data, "id = '$request[id]'");

       if($response > 0){
           $customer = DB()->select("u.email, u.fullname, t.book_type", "users as u, transaction as t", "u.id = t.user_id AND t.id = '$request[id]'")->get();

           $trans_type = ($customer['book_type'] == 0)?"Dine-in":"Takeout";

           $content = ($request['action'] == 'A')?"Your ".$trans_type." transaction was approved by the management":(($request['action'] == 'C')?"Your ".$trans_type." transaction was cancelled by the management. You can book again anytime with us. Thank you":(($request['action'] == 'F')?"Your ".$trans_type." transaction was set to completed. Thank you and come again":"Your ".$trans_type." transaction is on delivery. Our driver will contact the registered contact person for more details. Thank you"));
 
           $email = $this->sendEmail($customer['email'], $customer['fullname'], $content);
       }

       echo $email;
   }

   public function sendEmail($email_address, $company_name, $content)
    {
        $subject = "Drop By Restaurant - Reservation Status";

        $body = "<!DOCTYPE html>
                    <html>
                        <head>
                            <title></title>
                            <link rel='stylesheet' href='".public_url('/assets/adminty/bower_components/bootstrap/css/bootstrap.min.css')."'>
                        </head>
                        <body><h1>Hi ".ucwords($company_name).",</h1>
                    <p>".$content."</p>
                    
                    <p>Best Regards,<br />Management Team</p>
                    Please do not reply.
                        </body>
                    </html>";

        $recepients = $email_address;

        $sent = sendMail($subject, $body, $recepients);

        return $sent;
    }
}
