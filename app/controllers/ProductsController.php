<?php

namespace App\Controllers;
use App\Core\App;
use App\Core\Auth;
use App\Core\Request;
use App\Core\Filesystem;

class ProductsController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Individual Products";

        $products = DB()->selectLoop("*","individual_products")->get();

        return view('/products/index', compact('pageTitle', 'products'));
    }

    public function promo()
    {
        $pageTitle = "Promo Products";

        $products = DB()->selectLoop("*","promo_products")->get();

        return view('/products/indexPP', compact('pageTitle', 'products'));
    }

    public function addOns()
    {
        $pageTitle = "Add-ons";

        $products = DB()->selectLoop("*","addons")->get();

        return view('/products/addOns', compact('pageTitle', 'products'));
    }

    public function store()
    {
        $request = Request::validate();

        $data = [
            "product_name" => $request["pname"],
            "product_desc" => $request["pdesc"],
            "price" => $request["pprice"]
        ];

        $response = DB()->insert("individual_products", $data, "Y");

        echo $response;
    }

    public function storePP()
    {
        $request = Request::validate();

        $data = [
            "product_name" => $request["pname"],
            "product_desc" => $request["pdesc"],
            "price" => $request["pprice"]
        ];

        $response = DB()->insert("promo_products", $data, "Y");

        echo $response;
    }

    public function storeAO()
    {
        $request = Request::validate();

        $data = [
            "product_name" => $request["pname"],
            "product_desc" => $request["pdesc"],
            "price" => $request["pprice"]
        ];

        $response = DB()->insert("addons", $data, "Y");

        echo $response;
    }

    public function updateIP()
    {
        $request = Request::validate();

        $data = [
            "product_name" => $request["puname"],
            "product_desc" => $request["pudesc"],
            "price" => $request["puprice"]
        ];

        $response = DB()->update("individual_products", $data, "id = '$request[prodID]'");

        echo $response;
    }

    public function updatePP()
    {
        $request = Request::validate();

        $data = [
            "product_name" => $request["puname"],
            "product_desc" => $request["pudesc"],
            "price" => $request["puprice"]
        ];

        $response = DB()->update("promo_products", $data, "id = '$request[prodID]'");

        echo $response;
    }

    public function updateAO()
    {
        $request = Request::validate();

        $data = [
            "product_name" => $request["puname"],
            "product_desc" => $request["pudesc"],
            "price" => $request["puprice"]
        ];

        $response = DB()->update("addons", $data, "id = '$request[prodID]'");

        echo $response;
    }

    public function getIPDetails()
    {
        $request = Request::validate();

        $details = DB()->select("*","individual_products", "id = '$request[id]'")->get();

        $response = [];

        $response['name'] = $details['product_name'];
        $response['desc'] = $details['product_desc'];
        $response['price'] = $details['price'];

        echo json_encode($response);
    }

    public function getPPDetails()
    {
        $request = Request::validate();

        $details = DB()->select("*","promo_products", "id = '$request[id]'")->get();

        $response = [];

        $response['name'] = $details['product_name'];
        $response['desc'] = $details['product_desc'];
        $response['price'] = $details['price'];

        echo json_encode($response);
    }

    public function getAODetails()
    {
        $request = Request::validate();

        $details = DB()->select("*","addons", "id = '$request[id]'")->get();

        $response = [];

        $response['name'] = $details['product_name'];
        $response['desc'] = $details['product_desc'];
        $response['price'] = $details['price'];

        echo json_encode($response);
    }

    public function deleteIP()
    {
        $request = Request::validate();

        $response = DB()->delete("individual_products", "id = '$request[id]'");

        $checker = DB()->select("*", "user_uploads","product_id = '$request[id]' AND file_category = 'I'" )->get();
        if(!empty($checker['id'])){
            DB()->delete("user_uploads", "id = '$checker[id]' AND file_category = 'I'");
        }
        

        echo $response;
    }

    public function deletePP()
    {
        $request = Request::validate();

        $response = DB()->delete("promo_products", "id = '$request[id]'");

        $checker = DB()->select("*", "user_uploads","product_id = '$request[id]' AND file_category = 'PP'")->get();
        if(!empty($checker['id'])){
            DB()->delete("user_uploads", "id = '$checker[id]' AND file_category = 'PP'");
        }
        

        echo $response;
    }

    public function deleteAO()
    {
        $request = Request::validate();

        $response = DB()->delete("addons", "id = '$request[id]'");

        $checker = DB()->select("*", "user_uploads","product_id = '$request[id]' AND file_category = 'AO'")->get();
        if(!empty($checker['id'])){
            DB()->delete("user_uploads", "id = '$checker[id]' AND file_category = 'AO'");
        }
        

        echo $response;
    }

    public function changeStatusIP()
    {
        $request = Request::validate();
        $status = ($request['action'] == 'NA')?1:0;
        $data = [
            "status" => $status
        ];

        $response = DB()->update("individual_products", $data, "id = '$request[id]'");

        echo $response;
    }

    public function changeStatusPP()
    {
        $request = Request::validate();
        $status = ($request['action'] == 'NA')?1:0;
        $data = [
            "status" => $status
        ];

        $response = DB()->update("promo_products", $data, "id = '$request[id]'");

        echo $response;
    }

    public function changeStatusAO()
    {
        $request = Request::validate();
        $status = ($request['action'] == 'NA')?1:0;
        $data = [
            "status" => $status
        ];

        $response = DB()->update("addons", $data, "id = '$request[id]'");

        echo $response;
    }

    public function updateImageIP($id)
    {
        $user_id = Auth::user('id');
        //$file = new Filesystem;
      

        if(Request::hasFile('upload_file')){

            $checker = DB()->select("*", "user_uploads","product_id = '$id' AND file_category = 'I'")->get();

            if(!empty($checker['id'])){
                //if (Filesystem::exists($checker['slug'])) {
                    //$file->delete($checker['slug']);
                    DB()->delete("user_uploads", "id = '$checker[id]'");
               // }
            }

            
            $file_tmp = $_FILES['upload_file']['tmp_name'];
            $file_name = explode('_', str_replace(array('.', ' ', ',', '-'), '_', $_FILES['upload_file']['name']));
            array_pop($file_name);

            $fileSize = $_FILES['upload_file']['size'] / 1000000;
            $file_type = explode('.', $_FILES['upload_file']['name']);
            $file_type_end = end($file_type);
            $fileType = strtoupper($file_type_end);

            $filename = strtoupper(randChar(4)) . date('Ymdhis') . "." . $file_type_end;

            $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
            if (in_array($fileType, $image_icons_array)) {
                $previewSize = '100%';
            } else {
                $previewSize = '25%';
            }

            $folder = uniqid() . '-' . date('Ymdhis');
            $temp_dir = "public/assets/uploads/";

            $folderName = implode('_', $file_name);

            Request::storeAs($file_tmp, $temp_dir, $_FILES['upload_file']['type'], $filename);
            $save = $this->saveFilesInDbIP($user_id, $temp_dir . $filename, $folderName, $fileSize, $fileType, $previewSize, $id);
           
            
            echo $folderName;
        }
    }

    public function updateImagePP($id)
    {
        $user_id = Auth::user('id');
        //$file = new Filesystem;
      

        if(Request::hasFile('upload_file')){

            $checker = DB()->select("*", "user_uploads","product_id = '$id' AND file_category = 'PP'")->get();

            if(!empty($checker['id'])){
                //if (Filesystem::exists($checker['slug'])) {
                    //$file->delete($checker['slug']);
                    DB()->delete("user_uploads", "id = '$checker[id]'");
               // }
            }

            
            $file_tmp = $_FILES['upload_file']['tmp_name'];
            $file_name = explode('_', str_replace(array('.', ' ', ',', '-'), '_', $_FILES['upload_file']['name']));
            array_pop($file_name);

            $fileSize = $_FILES['upload_file']['size'] / 1000000;
            $file_type = explode('.', $_FILES['upload_file']['name']);
            $file_type_end = end($file_type);
            $fileType = strtoupper($file_type_end);

            $filename = strtoupper(randChar(4)) . date('Ymdhis') . "." . $file_type_end;

            $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
            if (in_array($fileType, $image_icons_array)) {
                $previewSize = '100%';
            } else {
                $previewSize = '25%';
            }

            $folder = uniqid() . '-' . date('Ymdhis');
            $temp_dir = "public/assets/uploads/";

            $folderName = implode('_', $file_name);

            Request::storeAs($file_tmp, $temp_dir, $_FILES['upload_file']['type'], $filename);
            $save = $this->saveFilesInDbPP($user_id, $temp_dir . $filename, $folderName, $fileSize, $fileType, $previewSize, $id);
           
            
            echo $folderName;
        }
    }

    public function updateImageAO($id)
    {
        $user_id = Auth::user('id');
        //$file = new Filesystem;
      

        if(Request::hasFile('upload_file')){

            $checker = DB()->select("*", "user_uploads","product_id = '$id' AND file_category = 'AO'")->get();

            if(!empty($checker['id'])){
                //if (Filesystem::exists($checker['slug'])) {
                    //$file->delete($checker['slug']);
                    DB()->delete("user_uploads", "id = '$checker[id]'");
               // }
            }

            
            $file_tmp = $_FILES['upload_file']['tmp_name'];
            $file_name = explode('_', str_replace(array('.', ' ', ',', '-'), '_', $_FILES['upload_file']['name']));
            array_pop($file_name);

            $fileSize = $_FILES['upload_file']['size'] / 1000000;
            $file_type = explode('.', $_FILES['upload_file']['name']);
            $file_type_end = end($file_type);
            $fileType = strtoupper($file_type_end);

            $filename = strtoupper(randChar(4)) . date('Ymdhis') . "." . $file_type_end;

            $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
            if (in_array($fileType, $image_icons_array)) {
                $previewSize = '100%';
            } else {
                $previewSize = '25%';
            }

            $folder = uniqid() . '-' . date('Ymdhis');
            $temp_dir = "public/assets/uploads/";

            $folderName = implode('_', $file_name);

            Request::storeAs($file_tmp, $temp_dir, $_FILES['upload_file']['type'], $filename);
            $save = $this->saveFilesInDbAO($user_id, $temp_dir . $filename, $folderName, $fileSize, $fileType, $previewSize, $id);
           
            
            echo $folderName;
        }
    }

    public function uploadFilePP($id)
    {
        $user_id = Auth::user('id');
        if(Request::hasFile('upload_file')){
            $file_tmp = $_FILES['upload_file']['tmp_name'];
            $file_name = explode('_', str_replace(array('.', ' ', ',', '-'), '_', $_FILES['upload_file']['name']));
            array_pop($file_name);

            $fileSize = $_FILES['upload_file']['size'] / 1000000;
            $file_type = explode('.', $_FILES['upload_file']['name']);
            $file_type_end = end($file_type);
            $fileType = strtoupper($file_type_end);

            $filename = strtoupper(randChar(4)) . date('Ymdhis') . "." . $file_type_end;

            $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
            if (in_array($fileType, $image_icons_array)) {
                $previewSize = '100%';
            } else {
                $previewSize = '25%';
            }

            $folder = uniqid() . '-' . date('Ymdhis');
            $temp_dir = "public/assets/uploads/";

            $folderName = implode('_', $file_name);

            Request::storeAs($file_tmp, $temp_dir, $_FILES['upload_file']['type'], $filename);
            $save = $this->saveFilesInDbPP($user_id, $temp_dir . $filename, $folderName, $fileSize, $fileType, $previewSize, $id);
           
            
            echo $folderName;
        }
    }

    public function uploadFileAO($id)
    {
        $user_id = Auth::user('id');
        if(Request::hasFile('upload_file')){
            $file_tmp = $_FILES['upload_file']['tmp_name'];
            $file_name = explode('_', str_replace(array('.', ' ', ',', '-'), '_', $_FILES['upload_file']['name']));
            array_pop($file_name);

            $fileSize = $_FILES['upload_file']['size'] / 1000000;
            $file_type = explode('.', $_FILES['upload_file']['name']);
            $file_type_end = end($file_type);
            $fileType = strtoupper($file_type_end);

            $filename = strtoupper(randChar(4)) . date('Ymdhis') . "." . $file_type_end;

            $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
            if (in_array($fileType, $image_icons_array)) {
                $previewSize = '100%';
            } else {
                $previewSize = '25%';
            }

            $folder = uniqid() . '-' . date('Ymdhis');
            $temp_dir = "public/assets/uploads/";

            $folderName = implode('_', $file_name);

            Request::storeAs($file_tmp, $temp_dir, $_FILES['upload_file']['type'], $filename);
            $save = $this->saveFilesInDbAO($user_id, $temp_dir . $filename, $folderName, $fileSize, $fileType, $previewSize, $id);
           
            
            echo $folderName;
        }
    }

    public function uploadFileIP($id)
    {
        $user_id = Auth::user('id');
        if(Request::hasFile('upload_file')){
            $file_tmp = $_FILES['upload_file']['tmp_name'];
            $file_name = explode('_', str_replace(array('.', ' ', ',', '-'), '_', $_FILES['upload_file']['name']));
            array_pop($file_name);

            $fileSize = $_FILES['upload_file']['size'] / 1000000;
            $file_type = explode('.', $_FILES['upload_file']['name']);
            $file_type_end = end($file_type);
            $fileType = strtoupper($file_type_end);

            $filename = strtoupper(randChar(4)) . date('Ymdhis') . "." . $file_type_end;

            $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
            if (in_array($fileType, $image_icons_array)) {
                $previewSize = '100%';
            } else {
                $previewSize = '25%';
            }

            $folder = uniqid() . '-' . date('Ymdhis');
            $temp_dir = "public/assets/uploads/";

            $folderName = implode('_', $file_name);

            Request::storeAs($file_tmp, $temp_dir, $_FILES['upload_file']['type'], $filename);
            $save = $this->saveFilesInDbIP($user_id, $temp_dir . $filename, $folderName, $fileSize, $fileType, $previewSize, $id);
           
            
            echo $folderName;
        }
    }  

    public function saveFilesInDbIP($user_id, $path, $file_name, $fileSize, $fileType, $previewSize, $id)
    {
      

        $data_form = [
            "user_id" => $user_id,
            "slug" => $path,
            "filetype" => $fileType,
            "filename" => $file_name,
            "filesize" => $fileSize,
            "iconsize" => $previewSize,
            "file_category" => "I",
            "product_id" => $id
        ];

        DB()->insert("user_uploads", $data_form);
    }

    public function saveFilesInDbPP($user_id, $path, $file_name, $fileSize, $fileType, $previewSize, $id)
    {
      

        $data_form = [
            "user_id" => $user_id,
            "slug" => $path,
            "filetype" => $fileType,
            "filename" => $file_name,
            "filesize" => $fileSize,
            "iconsize" => $previewSize,
            "file_category" => "PP",
            "product_id" => $id
        ];

        DB()->insert("user_uploads", $data_form);
    }

    public function saveFilesInDbAO($user_id, $path, $file_name, $fileSize, $fileType, $previewSize, $id)
    {
      

        $data_form = [
            "user_id" => $user_id,
            "slug" => $path,
            "filetype" => $fileType,
            "filename" => $file_name,
            "filesize" => $fileSize,
            "iconsize" => $previewSize,
            "file_category" => "AO",
            "product_id" => $id
        ];

        DB()->insert("user_uploads", $data_form);
    }
}
