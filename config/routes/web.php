<?php

/**
 * --------------------------------------------------------------------------
 * Routes
 * --------------------------------------------------------------------------
 * 
 * Here is where you can register routes for your application.
 * Now create something great!
 * 
 */

use App\Core\Routing\Route;

Route::get('/', function () {
    $pageTitle = "Welcome";
    return view('/welcome', compact('pageTitle'));
});

Route::get('/home', ['WelcomeController@home', ['auth']]);

Route::group(["prefix" => "products", "middleware" => ["auth"]], function () {
    Route::get('/individual', ['ProductsController@index']);
    Route::get('/promo', ['ProductsController@promo']);
    Route::get('/add-ons', ['ProductsController@addOns']);

    Route::post('/add-IP', ['ProductsController@store']);
    Route::post('/add-image-IP/{id}', ['ProductsController@uploadFileIP']);
    Route::post('/update-image-IP/{id}', ['ProductsController@updateImageIP']);
    Route::post('/get-product-details', ['ProductsController@getIPDetails']);
    Route::post('/update-IP', ['ProductsController@updateIP']);
    Route::post('/delete-IP', ['ProductsController@deleteIP']);
    Route::post('/change-status-IP', ['ProductsController@changeStatusIP']);

    Route::post('/add-PP', ['ProductsController@storePP']);
    Route::post('/add-image-PP/{id}', ['ProductsController@uploadFilePP']);
    Route::post('/get-product-details-PP', ['ProductsController@getPPDetails']);
    Route::post('/update-PP', ['ProductsController@updatePP']);
    Route::post('/change-status-PP', ['ProductsController@changeStatusPP']);
    Route::post('/update-image-PP/{id}', ['ProductsController@updateImagePP']);
    Route::post('/delete-PP', ['ProductsController@deletePP']);

    Route::post('/add-addons', ['ProductsController@storeAO']);
    Route::post('/add-image-AO/{id}', ['ProductsController@uploadFileAO']);
    Route::post('/get-product-details-AO', ['ProductsController@getAODetails']);
    Route::post('/update-AO', ['ProductsController@updateAO']);
    Route::post('/change-status-AO', ['ProductsController@changeStatusAO']);
    Route::post('/update-image-AO/{id}', ['ProductsController@updateImageAO']);
    Route::post('/delete-AO', ['ProductsController@deleteAO']);
});

Route::group(["prefix" => "menu", "middleware" => ["auth"]], function () {
    Route::get('/', ['MenuController@index']);

    Route::post('/search', ['MenuController@searchMenu']);
    Route::post('/add-to-order', ['MenuController@storeOrder']);
    Route::post('/product-details', ['MenuController@prodDetails']);
});

Route::group(["prefix" => "order", "middleware" => ["auth"]], function () {
    Route::get('/', ['OrderController@index']);
    Route::get('/finalize', ['OrderController@finalizeOrder']);

    Route::post('/change-quantity', ['OrderController@changeQntty']);
    Route::post('/finish-order', ['OrderController@finishOrder']);
    Route::post('/set-dine-in', ['OrderController@setDineIn']);
});

Route::group(["prefix" => "transaction", "middleware" => ["auth"]], function () {
    Route::get('/', ['TransactionController@index']);
    Route::get('/takeout', ['TransactionController@takeout']);
    Route::get('/dinein', ['TransactionController@dinein']);

    Route::post('/change-status', ['TransactionController@changeStatus']);
});

Route::group(["prefix" => "reports", "middleware" => ["auth"]], function () {
    Route::get('/', ['ReportController@index']);

    Route::post('/sales-report', ['ReportController@sales']);
});
