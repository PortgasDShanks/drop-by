<?php
use App\Core\Auth;

function checkIfImage($filetype)
{
    $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");

    if (in_array($filetype, $image_icons_array)) {
        $response = true;
    } else {
        $response = false;
    }

    return $response;
}

function getImageView($filetype, $path)
{
    $logo_path = public_url("/assets/sprnva/file_extension_icon/");
    $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
    $file_icons_array = array("XLS", "DOCX", "CSV", "TXT", "ZIP", "EXE", "XLSX", "PPT", "PPTX", "PDF");

    if (in_array($filetype, $image_icons_array)) {
        $icon = public_url("/../" . $path);
    } else {
        if (in_array($filetype, $file_icons_array)) {
            $icon = $logo_path . $filetype . '.png';
        } else {
            $icon = $logo_path . 'FILE.png';
        }
    }

    return $icon;
}

function getAvatar()
{
    $me = Auth::user('id');
    $avatar = DB()->select("slug", "user_uploads", "user_id = '$me' AND file_category = ''")->get();

    return (empty($avatar['slug']))?"":$avatar['slug'];
}

function getFileType()
{
    $me = Auth::user('id');
    $avatar = DB()->select("filetype", "user_uploads", "user_id = '$me' AND file_category = ''")->get();

    return (empty($avatar['filetype']))?'':$avatar['filetype'];
}

function getProdFileType($id)
{
    $avatar = DB()->select("filetype", "user_uploads", "product_id = '$id' AND file_category = 'I'")->get();

    return (empty($avatar['filetype']))?'':$avatar['filetype'];
}

function getProductImage($id)
{
    $avatar = DB()->select("slug", "user_uploads", "product_id = '$id' AND file_category = 'I'")->get();

    return (empty($avatar['slug']))?"":$avatar['slug'];
}


function getProdFileTypePP($id)
{
    $avatar = DB()->select("filetype", "user_uploads", "product_id = '$id' AND file_category = 'PP'")->get();

    return (empty($avatar['filetype']))?'':$avatar['filetype'];
}

function getProductImagePP($id)
{
    $avatar = DB()->select("slug", "user_uploads", "product_id = '$id' AND file_category = 'PP'")->get();

    return (empty($avatar['slug']))?"":$avatar['slug'];
}
function getProductImageAO($id)
{
    $avatar = DB()->select("slug", "user_uploads", "product_id = '$id' AND file_category = 'AO'")->get();

    return (empty($avatar['slug']))?"":$avatar['slug'];
}

function getProdFileTypeAO($id)
{
    $avatar = DB()->select("filetype", "user_uploads", "product_id = '$id' AND file_category = 'AO'")->get();

    return (empty($avatar['filetype']))?'':$avatar['filetype'];
}

function getPatientFileType($patient)
{

    $avatar = DB()->select("filetype", "user_uploads", "user_id = '$patient'")->get();

    return $avatar['filetype'];
}

function getAddress($latitude,$longitude){
    if(!empty($latitude) && !empty($longitude)){
        //Send request and receive json data by address
        $geocodeFromLatLong = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=10.6344115,122.952217&sensor=true&key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0'); 
        $output = json_decode($geocodeFromLatLong);
        $status = $output->status;
        //Get address from json data
        $address = ($status=="OK")?$output->results[2]->formatted_address:'';
        //Return address of the given latitude and longitude
        if(!empty($address)){
            return $address;
        }else{
            return $address;
        }
    }else{
        return false;   
    }
}

function getPendingOrderCount()
{
    $user = Auth::user('id');

    $counter = DB()->select("count(*) as c", "orders", " user_id = '$user' AND status = '0'")->get();

    return $counter['c'];
}

function getMenuName($category, $id)
{
    $table = ($category == 'I')?"individual_products":(($category == 'PP')?"promo_products":"addons");

    $data = DB()->select("product_name", $table, " id = '$id'")->get();

    return $data['product_name'];
}

function getMenuPrice($category, $id)
{
    $table = ($category == 'I')?"individual_products":(($category == 'PP')?"promo_products":"addons");

    $data = DB()->select("price", $table, " id = '$id'")->get();

    return $data['price'];
}

function getCurrentTotalAmount()
{
    $user = Auth::user('id');

    $counter = DB()->select("sum(quantity*price) as t", "orders", " user_id = '$user' AND status = '0'")->get();

    return $counter['t'];
}

function getCustomerName($id)
{
    $name = DB()->select("fullname","users","id='$id'")->get();

    return $name['fullname'];
}
