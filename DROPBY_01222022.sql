-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2022 at 04:58 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dropby`
--

-- --------------------------------------------------------

--
-- Table structure for table `addons`
--

CREATE TABLE `addons` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_desc` text DEFAULT NULL,
  `price` decimal(12,3) DEFAULT NULL,
  `status` int(1) DEFAULT 0,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `addons`
--

INSERT INTO `addons` (`id`, `product_name`, `product_desc`, `price`, `status`, `date_added`) VALUES
(1, 'test 2', 'test', '95.000', 1, '2021-11-24 08:46:58'),
(2, 'candybong', 'werwerwerwe', '5.000', NULL, '2021-11-24 08:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `individual_products`
--

CREATE TABLE `individual_products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_desc` varchar(255) DEFAULT NULL,
  `price` decimal(12,3) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `date_added` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `individual_products`
--

INSERT INTO `individual_products` (`id`, `product_name`, `product_desc`, `price`, `status`, `date_added`) VALUES
(1, 'test 2', 'test', '100.000', 1, '2021-11-18 15:17:30'),
(2, 'teasdasd', 'aasdas', '34.000', 0, '2021-11-18 15:46:29'),
(5, 'Sample product', 'dsfdsfsdfsdfsd', '100.000', 0, '2021-11-18 19:33:58'),
(6, 'Chicken Inasal', 'Inasal with no rice', '100.000', 0, '2021-11-30 12:43:25');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `migrations` text NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migrations`, `batch`) VALUES
(1, '20210408051901_create_password_resets_table', 1),
(2, '20210408051901_create_roles_table', 1),
(3, '20210408051901_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_item` int(11) NOT NULL,
  `order_category` varchar(5) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(12,3) NOT NULL,
  `header_id` int(11) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `order_item`, `order_category`, `quantity`, `price`, `header_id`, `status`) VALUES
(1, 2, 2, 'I', 3, '34.000', 1, 1),
(2, 2, 2, 'AO', 1, '5.000', 1, 1),
(3, 2, 2, 'AO', 1, '5.000', 1, 1),
(4, 3, 1, 'I', 3, '100.000', 3, 1),
(5, 3, 2, 'I', 4, '34.000', 4, 1),
(6, 4, 6, 'I', 3, '100.000', 5, 1),
(7, 4, 5, 'I', 3, '100.000', 5, 1),
(8, 4, 5, 'PP', 2, '180.000', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promo_products`
--

CREATE TABLE `promo_products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_desc` text NOT NULL,
  `price` decimal(12,3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `promo_products`
--

INSERT INTO `promo_products` (`id`, `product_name`, `product_desc`, `price`, `status`, `date_added`) VALUES
(4, 'tyrtyrtytrytrytry', 'ewrwfsdfsd', '120.000', 0, '2021-11-19 11:04:14'),
(5, 'xczxc', 'zxczxczx', '180.000', 0, '2021-11-19 11:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `role` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `book_type` int(1) NOT NULL COMMENT '0=Dinein,1=takeout',
  `contact_person` text NOT NULL,
  `contact_no` varchar(20) NOT NULL,
  `note` text NOT NULL,
  `status` int(1) NOT NULL COMMENT '0=pending,1=approved,2=deliver,3=completed,4=cancelled',
  `book_date` date NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id`, `user_id`, `book_type`, `contact_person`, `contact_no`, `note`, `status`, `book_date`, `date_added`) VALUES
(1, 2, 1, 'DASDASDASD', '12312321', 'ASDASDASDAS', 3, '2021-11-30', '2021-11-29 00:06:55'),
(2, 2, 0, 'test', '12312312', 'test', 1, '2021-12-02', '2021-11-29 17:23:33'),
(3, 3, 1, 'sadasdasdas', '12312312', 'sadfasd asdasdasda adsasdasd', 3, '2021-12-01', '2021-11-30 02:22:25'),
(4, 3, 1, 'test', '123123', 'adasdas dasdas', 0, '2021-12-01', '2021-11-30 02:23:46'),
(5, 4, 1, 'test', '797854115', 'test', 3, '2021-11-30', '2021-11-30 12:57:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `fullname` varchar(200) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `contact_no` varchar(20) NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `password` text NOT NULL,
  `role_id` varchar(1) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `fullname`, `address`, `contact_no`, `username`, `password`, `role_id`, `remember_token`, `updated_at`, `created_at`) VALUES
(1, 'dams2020@gmail.com', 'test test', NULL, '', 'vci', '$2y$10$9wCgQssF8HD5PxpCoF5mKOqRSxRPZSMbj/aMeGw0rPuGd.i8bHNi6', 'A', NULL, '2021-10-15 06:25:09', '2021-10-15 06:25:09'),
(2, 'ibayonabel@gmail.com', 'Kim Dahyun', 'Seoul South Korea', '', 'dahyun', '$2y$10$Rt71Hl5fz1XkeIvlQMzpSuGcYk6fL0viBHzCcqCGNjdbsswkzbRGu', 'U', NULL, '2021-11-17 07:40:15', '2021-11-17 07:40:15'),
(3, 'abelfullbusterbayon@gmail.com', 'Mina Myoui', 'test address', '', 'mina', '$2y$10$flAvtzwAIqbZzOKmqLTLp.1gQdBEFYT9rbzsGdueeq6t8jb0nIW8q', 'U', NULL, '2021-11-30 10:21:45', '2021-11-30 10:21:45'),
(4, 'villanuevajerico0869@gmail.com', 'Jerico villanueva', 'Bacolod City', '', 'jerico', '$2y$10$9LCQHvku5WWdMm.uYfh9aOyghL0wC2QaEsZRwmo8.dR.wPcYChLZC', 'U', NULL, '2021-11-30 20:52:33', '2021-11-30 20:52:33');

-- --------------------------------------------------------

--
-- Table structure for table `user_uploads`
--

CREATE TABLE `user_uploads` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `filetype` text NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filesize` text NOT NULL,
  `iconsize` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `file_category` varchar(5) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_uploads`
--

INSERT INTO `user_uploads` (`id`, `user_id`, `slug`, `filetype`, `filename`, `filesize`, `iconsize`, `created_at`, `file_category`, `product_id`) VALUES
(35, 1, 'public/assets/uploads/M1DM20211119054205.png', 'PNG', 'dropBy_logo', '0.222318', '100%', '2021-11-19 09:42:05', 'I', 1),
(42, 1, 'public/assets/uploads/PZFS20211123044526.jpg', 'JPG', '3', '0.013664', '100%', '2021-11-23 08:45:26', 'I', 2),
(45, 1, 'public/assets/uploads/77CO20211124045015.png', 'PNG', 'next', '0.016296', '100%', '2021-11-24 08:50:15', 'AO', 2),
(46, 1, 'public/assets/uploads/XRPG20211124045655.png', 'PNG', 'prev', '0.016559', '100%', '2021-11-24 08:56:55', 'AO', 1),
(48, 1, 'public/assets/uploads/P5RE20211130070549.jpg', 'JPG', '8', '0.055535', '100%', '2021-11-30 11:05:49', 'PP', 4),
(50, 1, 'public/assets/uploads/NDSW20211130070645.png', 'PNG', 'ab', '0.178776', '100%', '2021-11-30 11:06:45', 'PP', 5),
(53, 1, 'public/assets/uploads/OAKW20211130071027.jpg', 'JPG', '6', '0.026054', '100%', '2021-11-30 11:10:27', 'I', 5),
(55, 1, 'public/assets/uploads/UKFD20211130084739.jpg', 'JPG', 'g4', '0.127172', '100%', '2021-11-30 12:47:39', 'I', 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addons`
--
ALTER TABLE `addons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `individual_products`
--
ALTER TABLE `individual_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`email`) USING BTREE;

--
-- Indexes for table `promo_products`
--
ALTER TABLE `promo_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `user_uploads`
--
ALTER TABLE `user_uploads`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addons`
--
ALTER TABLE `addons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `individual_products`
--
ALTER TABLE `individual_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `promo_products`
--
ALTER TABLE `promo_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_uploads`
--
ALTER TABLE `user_uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
