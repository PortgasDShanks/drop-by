-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2021 at 11:59 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dropby`
--

-- --------------------------------------------------------

--
-- Table structure for table `addons`
--

CREATE TABLE `addons` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_desc` text DEFAULT NULL,
  `price` decimal(12,3) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `addons`
--

INSERT INTO `addons` (`id`, `product_name`, `product_desc`, `price`, `status`, `date_added`) VALUES
(1, 'test 2', 'test', '95.000', 1, '2021-11-24 08:46:58'),
(2, 'candybong', 'werwerwerwe', '5.000', NULL, '2021-11-24 08:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `individual_products`
--

CREATE TABLE `individual_products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_desc` varchar(255) DEFAULT NULL,
  `price` decimal(12,3) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `date_added` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `individual_products`
--

INSERT INTO `individual_products` (`id`, `product_name`, `product_desc`, `price`, `status`, `date_added`) VALUES
(1, 'test 2', 'test', '100.000', 1, '2021-11-18 15:17:30'),
(2, 'teasdasd', 'aasdas', '34.000', 0, '2021-11-18 15:46:29'),
(5, 'Sample product', 'dsfdsfsdfsdfsd', '100.000', 0, '2021-11-18 19:33:58');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `migrations` text NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migrations`, `batch`) VALUES
(1, '20210408051901_create_password_resets_table', 1),
(2, '20210408051901_create_roles_table', 1),
(3, '20210408051901_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_item` int(11) NOT NULL,
  `order_category` varchar(5) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(12,3) NOT NULL,
  `header_id` int(11) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promo_products`
--

CREATE TABLE `promo_products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_desc` text NOT NULL,
  `price` decimal(12,3) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `promo_products`
--

INSERT INTO `promo_products` (`id`, `product_name`, `product_desc`, `price`, `status`, `date_added`) VALUES
(4, 'tyrtyrtytrytrytry', 'ewrwfsdfsd', '120.000', 0, '2021-11-19 11:04:14'),
(5, 'xczxc', 'zxczxczx', '180.000', 0, '2021-11-19 11:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `role` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `fullname` varchar(200) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `contact_no` varchar(20) NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `password` text NOT NULL,
  `role_id` varchar(1) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `fullname`, `address`, `contact_no`, `username`, `password`, `role_id`, `remember_token`, `updated_at`, `created_at`) VALUES
(1, 'dams2020@gmail.com', 'test test', NULL, '', 'vci', '$2y$10$9wCgQssF8HD5PxpCoF5mKOqRSxRPZSMbj/aMeGw0rPuGd.i8bHNi6', 'A', NULL, '2021-10-15 06:25:09', '2021-10-15 06:25:09'),
(2, 'ibayonabel@gmail.com', 'Kim Dahyun', 'Seoul South Korea', '', 'dahyun', '$2y$10$Rt71Hl5fz1XkeIvlQMzpSuGcYk6fL0viBHzCcqCGNjdbsswkzbRGu', 'U', NULL, '2021-11-17 07:40:15', '2021-11-17 07:40:15');

-- --------------------------------------------------------

--
-- Table structure for table `user_uploads`
--

CREATE TABLE `user_uploads` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `filetype` text NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filesize` text NOT NULL,
  `iconsize` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `file_category` varchar(5) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_uploads`
--

INSERT INTO `user_uploads` (`id`, `user_id`, `slug`, `filetype`, `filename`, `filesize`, `iconsize`, `created_at`, `file_category`, `product_id`) VALUES
(34, 1, 'public/assets/uploads/C24M20211119033415.png', 'PNG', 'image', '0.544111', '100%', '2021-11-18 19:34:15', 'I', 5),
(35, 1, 'public/assets/uploads/M1DM20211119054205.png', 'PNG', 'dropBy_logo', '0.222318', '100%', '2021-11-19 09:42:05', 'I', 1),
(42, 1, 'public/assets/uploads/PZFS20211123044526.jpg', 'JPG', '3', '0.013664', '100%', '2021-11-23 08:45:26', 'I', 2),
(45, 1, 'public/assets/uploads/77CO20211124045015.png', 'PNG', 'next', '0.016296', '100%', '2021-11-24 08:50:15', 'AO', 2),
(46, 1, 'public/assets/uploads/XRPG20211124045655.png', 'PNG', 'prev', '0.016559', '100%', '2021-11-24 08:56:55', 'AO', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addons`
--
ALTER TABLE `addons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `individual_products`
--
ALTER TABLE `individual_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`email`) USING BTREE;

--
-- Indexes for table `promo_products`
--
ALTER TABLE `promo_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `user_uploads`
--
ALTER TABLE `user_uploads`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addons`
--
ALTER TABLE `addons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `individual_products`
--
ALTER TABLE `individual_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promo_products`
--
ALTER TABLE `promo_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_uploads`
--
ALTER TABLE `user_uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
